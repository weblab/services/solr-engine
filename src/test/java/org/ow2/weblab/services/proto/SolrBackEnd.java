/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.proto;

import java.io.File;
import java.rmi.ServerException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.SolrJettyTestBase;
import org.apache.solr.client.solrj.embedded.JettySolrRunner;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.Document;

public class SolrBackEnd {

	private static int NB_DOCS = 30;

	private static boolean ONLINE = false;

	protected static final Log LOGGER = LogFactory.getLog(SolrBackEnd.class);

	private static final String SOLR_SERVER_CONTEXT = "/core_default";

	private static final String TEST_HOME = "target/test-classes/solr/server/solr/";
	private static final String SOLR_CONFIG = "target/test-classes/solr/cores/core_default/solrconfig.xml";
	private static final String DATA_DIR = "target/data";
	private static JettySolrRunner jetty;

	public static void main(final String[] args) throws InterruptedException, SolrProtoException, ServerException {
		LOGGER.info("Launching Solr server...");
		SolrBackEnd.launchServer();
		LOGGER.info("Solr server launched on " + getURL());

		int myCpt = 20;

		while (myCpt-- > 0) {
			LOGGER.info((myCpt) + " seconds left...");
			Thread.sleep(1000);
		}
		LOGGER.info("Stoping Solr server on " + getURL());
		SolrBackEnd.stopServer();
	}

	public static String getURL() throws ServerException {
		if (jetty != null) {
			return "http://localhost:" + jetty.getLocalPort() + SOLR_SERVER_CONTEXT;
		}
		throw new ServerException("Solr server not started.");
	}

	public static void launchServer() throws SolrProtoException {
		if (jetty == null) {
			if (System.getProperty("solr.data.dir") == null) {
				System.setProperty("solr.data.dir", DATA_DIR);
			}

			try {
//			    Properties nodeProps = new Properties();
//			    nodeProps.setProperty("coreRootDirectory", "target/test-classes/solr/cores/");
//			    nodeProps.setProperty("configSetBaseDir", "test-classes/solr/");
				jetty = SolrJettyTestBase.createJetty(TEST_HOME, SOLR_CONFIG, SOLR_SERVER_CONTEXT);
//			    Properties nodeProps = new Properties();
//			    nodeProps.setProperty("coreRootDirectory", "target/test-classes/solr/cores/");
//			    nodeProps.setProperty("configSetBaseDir", "test-classes/solr/");
//
//			    JettyConfig jettyConfig = JettyConfig.builder()
//			            .setContext("")
//			            .setPort(1212)
//			            .stopAtShutdown(true)
//			            .withServlets(null)
//			            .withSSLConfig(null)
//			            .build();
//			    jetty = new JettySolrRunner(TEST_HOME, nodeProps, jettyConfig);
//			    jetty.start();
			    //port = jetty.getLocalPort();
				LOGGER.info("Solr server launched on " + getURL());
			} catch (Exception e) {
				SolrBackEnd.LOGGER.error("Error while launching jetty container for Solr during tests: " + e);
				throw new SolrProtoException("Error while launching jetty container for Solr during tests.", e);
			}
		}
	}


	public static void stopServer() throws SolrProtoException {
		if (jetty != null) {
			try {
				jetty.stop();
			} catch (Exception e) {
				SolrBackEnd.LOGGER.error("Unable to stop the jetty server: " + e);
				throw new SolrProtoException("Unable to stop the jetty server.", e);
			}
			jetty = null;
		}
		deleteServerData();
	}


	private static void deleteServerData() {
		FileUtils.deleteQuietly(new File(DATA_DIR));
		FileUtils.deleteQuietly(new File(TEST_HOME + "/core_default"));
		// TODO check but it's probably not usefull anymore
		for (final File file : new File("target").listFiles()) {
			if (file.isDirectory() && file.toString().contains("core")) {
				FileUtils.deleteQuietly(file);
			}
		}
		SolrBackEnd.LOGGER.trace("SolR data deleted.");
	}

	public static List<Document> getDocs() {
		final WebLabMarshaller m = new WebLabMarshaller();
		final List<Document> docs = new LinkedList<>();
		if (SolrBackEnd.ONLINE) {
			int nbDoc = SolrBackEnd.NB_DOCS;
			while (nbDoc-- > 0) {
				final Document d = DocumentCreator.createDocumentFromGuttenbergEbooks();
				docs.add(d);
				try {
					m.marshalResource(d, new File("./src/test/resources/samples/doc" + d.getUri().hashCode() + ".xml"));
				} catch (final WebLabCheckedException e) {
					SolrBackEnd.LOGGER.warn("Cannot save document [" + d.getUri() + "].");
				}
			}
		} else {
			for (final File f : new File("src/test/resources/samples/").listFiles()) {
				if (f.isFile()) {
					try {
						docs.add(m.unmarshal(f, Document.class));
					} catch (final WebLabCheckedException e) {
						SolrBackEnd.LOGGER.warn("Cannot load document [" + f + "].");
					}
				}
			}
		}
		return docs;
	}


	public static class SolrProtoException extends Exception {
		public SolrProtoException(String msg, Exception e) {
			super(msg, e);
		}

		private static final long serialVersionUID = 1L;

	}

}
