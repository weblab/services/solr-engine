/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.searcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Callable;

import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.Searcher;
import org.ow2.weblab.core.services.searcher.SearchArgs;

public class SolrSearchThread implements Callable<SolrSearchThread> {


	private int max = 100;


	private long totalTime = 0;


	private final int id;


	private final SolrSearcher solrSearcher;


	public SolrSearchThread(final SolrSearcher solrSearcher, final int id, final int nbQueries) {
		this.solrSearcher = solrSearcher;
		this.max = nbQueries;
		this.id = id;
	}


	@Override
	public SolrSearchThread call() {
		LogFactory.getLog(this.getClass()).debug("Thread [" + this.id + "] starts");

		final File propsFile = new File("src/test/resources/queries_gen.txt");
		final Properties props = new Properties();
		try {
			props.load(new FileInputStream(propsFile));
		} catch (IOException ioe) {
			throw new WebLabUncheckedException(ioe);
		}

		// Searcher service = new FastSolrSearcher();
		final Searcher service = this.solrSearcher; // new SolrSearcher();

		final SearchArgs args = new SearchArgs();
		final StringQuery q = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);

		args.setQuery(q);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		for (int i = 0; i < this.max; i++) {
			q.setRequest(props.getProperty("q" + (i - (i % props.size()))));

			final long startTime = System.currentTimeMillis();

			try {
				service.search(args);
			} catch (Exception e) {
				throw new WebLabUncheckedException(e);
			}

			final long finalTime = System.currentTimeMillis();

			this.totalTime += finalTime - startTime;
		}

		LogFactory.getLog(this.getClass()).debug("average time : " + (this.totalTime / this.max) + "ms");
		LogFactory.getLog(this.getClass()).debug("Thread [" + this.id + "] ends");
		return this;
	}


	public long getTotalTime() {
		return this.totalTime;
	}

}
