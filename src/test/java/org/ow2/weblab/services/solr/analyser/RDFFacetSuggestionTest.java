/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.ow2.weblab.services.solr.searcher.SolrSearcher;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class RDFFacetSuggestionTest {


	private static RDFFacetSuggestion rdfFacetSuggestion;


	private static SolrIndexer solrIndexer;


	private static SolrSearcher solrSearcher;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();
		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			RDFFacetSuggestionTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			RDFFacetSuggestionTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			RDFFacetSuggestionTest.solrIndexer.init();

			RDFFacetSuggestionTest.rdfFacetSuggestion = factory.getBean(RDFFacetSuggestion.BEAN_NAME, RDFFacetSuggestion.class);
			RDFFacetSuggestionTest.rdfFacetSuggestion.init();

			RDFFacetSuggestionTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			RDFFacetSuggestionTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			RDFFacetSuggestionTest.solrSearcher.init();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() {
		// SolrComponentInstanceTest.end();
	}


	@Test
	public void multipleDocTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			RDFFacetSuggestionTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(RDFFacetSuggestionTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("a*");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = RDFFacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) RDFFacetSuggestionTest.rdfFacetSuggestion.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		Assert.assertEquals(6, set.getResource().size());
	}
}
