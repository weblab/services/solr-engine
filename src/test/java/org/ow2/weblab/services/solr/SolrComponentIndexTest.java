/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;

@Ignore
public class SolrComponentIndexTest {


	public static final Pattern nbDocInCoreStatusPattern = Pattern.compile(".*numDocs=(\\d*),.*");


	private static final Pattern emptyCoreStatusPattern = Pattern.compile(".*=\\{\\}\\}\\}.*");


	private static final Log LOGGER = LogFactory.getLog(SolrComponentInstanceTest.class);


	private static final String TO_BE_INDEXED = "src/test/resources/ebooks";


	private static SolrComponent comp;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();
		SolrComponentIndexTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
	}


	@AfterClass
	public static void end() throws SolrProtoException {
		SolrComponentIndexTest.comp.commit();
		SolrComponentInstanceTest.end();
	}


	@Test
	public void addOneDocument() throws Exception {
		String status = SolrComponentIndexTest.comp.getStatus();
		final Matcher empty = SolrComponentIndexTest.emptyCoreStatusPattern.matcher(status);
		Matcher m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		int nbDoc = 0;
		if (!empty.matches()) {
			// core not empty trying to get nb Doc
			if (!m.matches()) {
				// OoooOh that's strange
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			} else {
				// getting nb doc
				nbDoc = Integer.parseInt(m.group(1));
			}
		}

		final SolrInputDocument doc = SolrComponentIndexTest.createStaticSimpleDoc();

		SolrComponentIndexTest.comp.addDocument(doc);
		SolrComponentIndexTest.comp.commit();

		status = SolrComponentIndexTest.comp.getStatus();
		SolrComponentIndexTest.LOGGER.info(status);

		m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {
			Assert.assertEquals(nbDoc + 1, Integer.parseInt(m.group(1)));
		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}


	@Test(expected = WebLabCheckedException.class)
	public void addOneBadDocument() throws WebLabCheckedException {
		String status = SolrComponentIndexTest.comp.getStatus();
		final Matcher empty = SolrComponentIndexTest.emptyCoreStatusPattern.matcher(status);
		final Matcher m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		if (!empty.matches()) {
			// core not empty trying to get nb Doc
			if (!m.matches()) {
				// OoooOh that's strange
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			}
		}

		final SolrInputDocument doc = SolrComponentIndexTest.createStaticSimpleDoc();

		// adding extra id to make it bad formatted doc
		doc.addField("id", "http://en.wikipedia.org/wiki/Bellman_equation/thatIsBad");

		SolrComponentIndexTest.comp.addDocument(doc);
		SolrComponentIndexTest.comp.commit();

		status = SolrComponentIndexTest.comp.getStatus();
		SolrComponentIndexTest.LOGGER.info(status);

		Assert.fail("A exception should be raised.");
	}


	@Test
	public void addSomeDocuments() throws WebLabCheckedException, IOException {
		String status = SolrComponentIndexTest.comp.getStatus();
		final Matcher empty = SolrComponentIndexTest.emptyCoreStatusPattern.matcher(status);
		Matcher m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		int nbDoc = 0;
		if (!empty.matches()) {
			// core not empty trying to get nb Doc
			if (!m.matches()) {
				// OoooOh that's strange
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			} else {
				// getting nb doc
				nbDoc = Integer.parseInt(m.group(1));
			}
		}

		final List<SolrInputDocument> docs = SolrComponentIndexTest.getEbooksAsSolrInputDocument();

		final int nbDocToIndex = docs.size();

		SolrComponentIndexTest.LOGGER.info("Let's start to index !");
		for (final SolrInputDocument doc : docs) {
			SolrComponentIndexTest.comp.addDocument(doc);
		}

		SolrComponentIndexTest.comp.commit();

		status = SolrComponentIndexTest.comp.getStatus();
		SolrComponentIndexTest.LOGGER.info(status);

		m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {
			Assert.assertEquals(nbDoc + nbDocToIndex, Integer.parseInt(m.group(1)));
		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}


	public static List<SolrInputDocument> getEbooksAsSolrInputDocument() throws IOException {
		final List<SolrInputDocument> docs = new LinkedList<>();
		final File file = new File(SolrComponentIndexTest.TO_BE_INDEXED);
		for (final File f : file.listFiles()) {
			if (f.getName().endsWith(".txt")) {
				final SolrInputDocument doc = new SolrInputDocument();
				doc.addField(SolrConfig.FIELD_ID, f.toURI().toString());
				final StringWriter writer = new StringWriter();
				try (final Reader reader = new BufferedReader(new FileReader(f))) {
					final char[] cbuf = new char[1024];
					while (reader.read(cbuf) != -1) {
						writer.write(cbuf);
					}
				}
				doc.addField(SolrConfig.FIELD_TEXT, writer.toString());
				docs.add(doc);
			}
		}
		return docs;
	}


	public static SolrInputDocument createStaticSimpleDoc() {
		final SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", "http://en.wikipedia.org/wiki/Bellman_equation");
		doc.addField("text", "A Bellman equation (also known as a dynamic programming equation), named after its discoverer, Richard Bellman, is a "
				+ "necessary condition for optimality associated with the mathematical optimization method known as dynamic programming. "
				+ "It breaks a dynamic optimization problem into simpler subproblems, writing the value of a decision problem at a "
				+ "certain point in time in terms of the payoff from some initial choices and the value of the remaining decision problem that "
				+ "results from those initial choices, as Bellman's Principle of Optimality prescribes.\n The Bellman equation was first applied "
				+ "to engineering control theory and to other topics in applied mathematics, and subsequently became an important tool in "
				+ "economic theory. n Almost any problem which can be solved using optimal control theory can also be solved by analyzing the "
				+ "appropriate Bellman equation. However, the term 'Bellman equation' usually refers to the dynamic programming equation associated with "
				+ "discrete-time optimization problems. In continuous-time optimization problems, the analogous equation is a partial "
				+ "differential equation which is usually called the Hamilton-Jacobi-Bellman equation.");
		return doc;
	}
}
