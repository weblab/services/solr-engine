/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.ow2.weblab.services.solr.searcher.SolrSearcher;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class DeduplicateTest {
	private static Deduplicate deduplicateService;

	private static SolrIndexer solrIndexer;

	private static SolrSearcher solrSearcher;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();
		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			DeduplicateTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			DeduplicateTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			DeduplicateTest.solrIndexer.init();

			DeduplicateTest.deduplicateService = factory.getBean(Deduplicate.BEAN_NAME, Deduplicate.class);

			DeduplicateTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			DeduplicateTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			DeduplicateTest.solrSearcher.init();
		}

	}


	@AfterClass
	public static void deleteTestingIndex() {
		// SolrComponentInstanceTest.end();
	}

	@Test
	public void simpleTest() throws Exception {
		final Document doc = DocumentCreator.createDocumentWithAnnotation();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(doc);

		// check document is not dupliacte in index
		Document processedDoc = (Document) DeduplicateTest.deduplicateService.process(pargs).getResource();
		WProcessingAnnotator wProcessingAnnotator = new WProcessingAnnotator(processedDoc);
		Value<Boolean> value = wProcessingAnnotator.readCanBeIgnored();
		if(value != null){
			Assert.assertEquals(0, value.getValues().size());
		}

		//index
		final IndexArgs iargs = new IndexArgs();
		iargs.setResource(doc);
		DeduplicateTest.solrIndexer.index(iargs);

		SolrComponent.getInstance(DeduplicateTest.solrIndexer.getConf(), null).commit();

		// check document is marked duplicate now
		processedDoc = (Document) DeduplicateTest.deduplicateService.process(pargs).getResource();
		wProcessingAnnotator = new WProcessingAnnotator(processedDoc);
		value = wProcessingAnnotator.readCanBeIgnored();
		if(value != null){
			Assert.assertEquals(1, value.getValues().size());
			Assert.assertTrue(value.getValues().get(0).booleanValue());
		}

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(processedDoc));
	}

}
