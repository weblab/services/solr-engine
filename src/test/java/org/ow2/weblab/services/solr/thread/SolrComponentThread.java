/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.thread;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.apache.commons.logging.LogFactory;
import org.apache.solr.common.SolrInputDocument;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentIndexTest;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;

public class SolrComponentThread implements Callable<SolrComponentThread> {


	private SolrComponent comp;


	private final int id;


	private final int cpt = 0;


	private long durationSum;


	public SolrComponentThread(final int id) {
		this.id = id;
	}


	@Override
	public SolrComponentThread call() {
		LogFactory.getLog(this.getClass()).debug("Running [" + this.getClass().getSimpleName() + "] : " + this.id + "...");
			this.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
		try {
			for (final SolrInputDocument doc : SolrComponentIndexTest.getEbooksAsSolrInputDocument()) {
				this.comp.addDocument(doc);
			}
		} catch (final WebLabCheckedException e) {
			throw new WebLabUncheckedException("Indexing error", e);
		} catch (final IOException e) {
			throw new WebLabUncheckedException("Indexing error", e);
		}

		this.comp.commit();

		LogFactory.getLog(this.getClass()).debug(("[" + this.getClass().getSimpleName() + "] " + this.id + " is done."));
		return this;
	}


	public int getCpt() {
		return this.cpt;
	}


	public long getMeanDurationTime() {
		if (this.cpt == 0) {
			return Integer.MAX_VALUE;
		}
		return this.durationSum / this.cpt;
	}


	public long getDurationSum() {
		return this.durationSum;
	}


	public int getId() {
		return this.id;
	}

}
