/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.dataoperator;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.cleanable.CleanArgs;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexerTest;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrDataOperatorTest {


	private static SolrDataOperator solrDataOperator;


	@BeforeClass
	public static void setUp() throws Exception {
		final SolrIndexerTest indexTest = new SolrIndexerTest();
		SolrIndexerTest.start();
		indexTest.indexSomeDocuments();


		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {

			SolrDataOperatorTest.solrDataOperator = factory.getBean(SolrDataOperator.BEAN_NAME, SolrDataOperator.class);
	//		SolrDataOperatorTest.solrDataOperator.getConf().setSolrHome(
	//				new File(SolrDataOperatorTest.solrDataOperator.getConf().getSolrHome()).getAbsoluteFile().getAbsolutePath());
	//		SolrDataOperatorTest.solrDataOperator.getConf().setSolrData(
	//				new File(SolrDataOperatorTest.solrDataOperator.getConf().getSolrData()).getAbsoluteFile().getAbsolutePath());
	//		SolrDataOperatorTest.solrDataOperator.init();
			SolrComponent.getInstance(SolrDataOperatorTest.solrDataOperator.getConf(), null).commit();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	public void cleanOneDoc() throws WebLabCheckedException, ServiceNotConfiguredException, InsufficientResourcesException, UnexpectedException,
			InvalidParameterException, AccessDeniedException, ContentNotAvailableException, UnsupportedRequestException {
		final CleanArgs args = new CleanArgs();

		final String uri = SolrDataOperatorTest.getOneDocURI();
		args.getTrash().add(uri);

		SolrDataOperatorTest.solrDataOperator.clean(args);
	}


	private static String getOneDocURI() throws WebLabCheckedException {
		final File folder = new File(SolrIndexerTest.SAMPLES_TO_BE_INDEXED);
		final WebLabMarshaller m = new WebLabMarshaller();
		String uri = null;
		final Iterator<File> files = Arrays.asList(folder.listFiles()).iterator();
		while (uri == null) {
			final File f = files.next();
			if (f.getName().endsWith(".xml")) {
				final Document doc = m.unmarshal(f, Document.class);
				uri = doc.getUri();
			}
		}
		return uri;
	}
}
