/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.util.Set;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.ow2.weblab.services.solr.searcher.SolrSearcher;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class HighlighterTest {


	private static Highlighter highlighter;


	private static SolrIndexer solrIndexer;


	private static SolrSearcher solrSearcher;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			HighlighterTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			HighlighterTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			HighlighterTest.solrIndexer.init();

			HighlighterTest.highlighter = factory.getBean(Highlighter.BEAN_NAME, Highlighter.class);
			HighlighterTest.highlighter.init();

			HighlighterTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			HighlighterTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			HighlighterTest.solrSearcher.init();
		}


	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	public void simpleTest() throws Exception {
		final Document doc = DocumentCreator.createDocumentWithAnnotation();

		final IndexArgs iargs = new IndexArgs();
		iargs.setResource(doc);
		HighlighterTest.solrIndexer.index(iargs);

		SolrComponent.getInstance(HighlighterTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("text");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = HighlighterTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) HighlighterTest.highlighter.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok());

		for (final String hitUri : hlpr.getSubjsOnPred(WebLabRetrieval.HAS_RANK)) {
			Assert.assertTrue(hlpr.getLitsOnPredSubj(hitUri, WebLabRetrieval.HAS_DESCRIPTION).size() > 0);
		}
	}


	@Test
	public void multipleDocTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			HighlighterTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(HighlighterTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("text");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = HighlighterTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) HighlighterTest.highlighter.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok());

		for (final String hitUri : hlpr.getSubjsOnPred(WebLabRetrieval.HAS_RANK)) {
			Assert.assertTrue(hlpr.getLitsOnPredSubj(hitUri, WebLabRetrieval.HAS_DESCRIPTION).size() > 0);
		}
	}


	@Test
	public void emptyQueryTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			HighlighterTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(HighlighterTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("*:*");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = HighlighterTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) HighlighterTest.highlighter.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok());
		for (final String hitUri : hlpr.getSubjsOnPred(WebLabRetrieval.HAS_RANK)) {
			Assert.assertTrue(hlpr.getLitsOnPredSubj(hitUri, WebLabRetrieval.HAS_DESCRIPTION).size() > 0);
		}

	}


	@Test
	public void highlightOnOrderByQueryTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			HighlighterTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(HighlighterTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("*:*");
		new WRetrievalAnnotator(query).writeToBeRankedBy(URI.create(WebLabProcessing.HAS_GATHERING_DATE));

		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = HighlighterTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) HighlighterTest.highlighter.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok());
		final Set<String> hits = hlpr.getSubjsOnPred(WebLabRetrieval.HAS_RANK); // TODO Use of WRetrievalAnnotator when possible
		LogFactory.getLog(this.getClass()).debug(hits);
		int cpt = 0;
		for (final String hitUri : hits) {
			if (hlpr.getLitsOnPredSubj(hitUri, WebLabRetrieval.HAS_DESCRIPTION).size() > 0) {
				cpt++;
			}
		}
		Assert.assertEquals(hits.size(), cpt);
	}
}
