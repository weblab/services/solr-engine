/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.ow2.weblab.services.solr.searcher.SolrSearcher;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class FacetSuggestionTest {


	private static FacetSuggestion facetSuggestion;


	private static SolrIndexer solrIndexer;


	private static SolrSearcher solrSearcher;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();
		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			FacetSuggestionTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			FacetSuggestionTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			FacetSuggestionTest.solrIndexer.init();

			FacetSuggestionTest.facetSuggestion = factory.getBean(FacetSuggestion.BEAN_NAME, FacetSuggestion.class);
			FacetSuggestionTest.facetSuggestion.init();

			FacetSuggestionTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			FacetSuggestionTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			FacetSuggestionTest.solrSearcher.init();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() {
		// SolrComponentInstanceTest.end();
	}


	@Test
	public void simpleTest() throws Exception {
		final Document doc = DocumentCreator.createDocumentWithAnnotation();

		final IndexArgs iargs = new IndexArgs();
		iargs.setResource(doc);
		FacetSuggestionTest.solrIndexer.index(iargs);

		SolrComponent.getInstance(FacetSuggestionTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("text");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = FacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) FacetSuggestionTest.facetSuggestion.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		Assert.assertEquals(2, set.getResource().size());
	}

	@Test
	public void testFormatFacet() throws Exception {
		//This test looks into the facet results to find that dc:format based facets
		//returns a correct value

		// First, we index all test documents
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			FacetSuggestionTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(FacetSuggestionTest.solrIndexer.getConf(), null).commit();

		//Then search for them
		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("a*");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = FacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) FacetSuggestionTest.facetSuggestion.process(pargs).getResource();

		//new WebLabMarshaller().marshalResource(set, System.out);

		// Now parse the resultSet returned to find format facet
		//Retrieve all the statements
		ResourceHelper helper = new JenaResourceHelper(set);

		// Look for queries with hasScope property on dc:format
		Set<String> uris = helper.getSubjsOnPredLit(WebLabRetrieval.HAS_SCOPE, DublinCore.FORMAT_PROPERTY_NAME);

		Map<String, StringQuery> queries = new HashMap<>();
		for (final StringQuery q : ResourceUtil.getSelectedSubResources(set, StringQuery.class)) {
			queries.put(q.getUri(), q);
		}

		//For each, check the value which is embedded inside the request
		for (String uri : uris)
		{

			StringQuery filter = queries.get(uri);
			Assert.assertNotNull(filter);
			String format = filter.getRequest().replaceAll("\"", "");

			// Test if the format returned matches mime-types like
			// application/pdf
			// application/xhtml+xml
			// charset=utf-8 (coming from two parts mime)
			Assert.assertTrue(format.matches("(.+/.+)|(.+=.+)"));
		}
	}

	@Test
	public void multipleDocTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			FacetSuggestionTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(FacetSuggestionTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("a*");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = FacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) FacetSuggestionTest.facetSuggestion.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		Assert.assertEquals(2, set.getResource().size());
	}


	@Test
	public void onOrderByQueryTest() throws Exception {
		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			FacetSuggestionTest.solrIndexer.index(iargs);
		}
		SolrComponent.getInstance(FacetSuggestionTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("a*");
		new WRetrievalAnnotator(query).writeToBeRankedBy(URI.create(WebLabProcessing.HAS_GATHERING_DATE));

		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = FacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) FacetSuggestionTest.facetSuggestion.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		Assert.assertEquals(2, set.getResource().size());
	}


	@Ignore("find a valid dataset to test this")
	// TODO find a valid dataset to test this
	@Test
	public void validComposedQuery() throws Exception {
		this.multipleDocTest();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query0" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("*:*");

		final StringQuery facet = WebLabResourceFactory.createResource("test", "query1" + System.currentTimeMillis(), StringQuery.class);
		facet.setRequest("http\\://www.gutenberg.org/*");
		new WRetrievalAnnotator(query).writeToBeRankedBy(URI.create(DublinCore.SOURCE_PROPERTY_NAME));

		final ComposedQuery cquery = WebLabResourceFactory.createResource("test", "query3" + System.currentTimeMillis(), ComposedQuery.class);
		cquery.setOperator(Operator.AND);

		cquery.getQuery().add(query);
		cquery.getQuery().add(facet);

		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(cquery);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = FacetSuggestionTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) FacetSuggestionTest.facetSuggestion.process(pargs).getResource();

		Assert.assertNotNull(set);
		Assert.assertNotNull(set.getResource());
		Assert.assertEquals(2, set.getResource().size());

		final Resource cres = set.getResource().get(1);

		Assert.assertNotNull(cres);

		if (!(cres instanceof ComposedResource)) {
			Assert.fail("The second resource of the result set should be a ComposedResource.");
		} else {
			final ComposedResource facetQueries = (ComposedResource) cres;

			Assert.assertNotNull(facetQueries.getResource());
			Assert.assertTrue(facetQueries.getResource().size() > 0);

			for (final Resource r : facetQueries.getResource()) {
				if (!(r instanceof ComposedQuery)) {
					Assert.fail("Queries coming from facet suggestion should be ComposedQuery.");
				} else {
					final ComposedQuery cQuery = (ComposedQuery) r;
					boolean flag = false;
					for (final Query q : cQuery.getQuery()) {
						if (q.getUri().equals(query.getUri())) {
							flag = true;
						} else {
							Assert.assertEquals(1, q.getAnnotation().size());
							final Value<URI> scope = new WRetrievalAnnotator(q).readScope();
							Assert.assertNotNull(scope);
							Assert.assertTrue(scope.hasValue());
							Assert.assertEquals(1, scope.size());
						}
					}
					if (!flag) {
						Assert.fail("Original query should be kept in composedQuery from facet suggestion.");
					}
				}
			}
		}
	}

}
