/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.indexer;

import java.io.File;
import java.net.URI;
import java.util.UUID;
import java.util.regex.Matcher;

import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentIndexTest;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.util.index.Field;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrIndexerTest {


	public static final String SAMPLES_TO_BE_INDEXED = "src/test/resources/samples";


	private static SolrIndexer solrIndexer;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			SolrIndexerTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);

			SolrIndexerTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());

			SolrIndexerTest.solrIndexer.init();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}

	@Test
	public void multipleValuePropertyFilter() throws WebLabCheckedException{
		final WebLabMarshaller m = new WebLabMarshaller();

		final Document doc = m.unmarshal(new File("src/test/resources/samples/0c113fb0f6.xml"), Document.class);

		SolrInputDocument solrDoc = solrIndexer.convertMediaUnit(doc);

		SolrInputField field =  solrDoc.getField("created");

		Assert.assertNotNull(field);

		Assert.assertEquals(1, field.getValueCount());
		Assert.assertEquals("2011-09-09T08:28:08.000Z", field.getFirstValue());
	}


	@Test
	public void index() throws InvalidParameterException, UnexpectedException, WebLabCheckedException, ServiceNotConfiguredException {
		SolrComponent.getInstance(SolrIndexerTest.solrIndexer.getConf(), null).commit();

		String status = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).getStatus();
		Matcher m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {
			final int nbDoc = Integer.parseInt(m.group(1));
			final Document doc = DocumentCreator.createDocWithDeutsch();

			final IndexArgs args = new IndexArgs();
			args.setResource(doc);

			SolrIndexerTest.solrIndexer.index(args);
			SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).commit();
			status = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).getStatus();

			m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
			if (m.matches()) {
				Assert.assertEquals(nbDoc + 1, Integer.parseInt(m.group(1)));
			} else {
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			}
		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}

	@Test
	public void indexSomeDocuments() throws UnexpectedException, InvalidParameterException, ServiceNotConfiguredException, WebLabCheckedException {

		final File folder = new File(SolrIndexerTest.SAMPLES_TO_BE_INDEXED);
		final WebLabMarshaller m = new WebLabMarshaller();

		final IndexArgs args = new IndexArgs();

		for (final File f : folder.listFiles()) {
			if (f.getName().endsWith(".xml")) {
				LogFactory.getLog(this.getClass()).debug(f);
				final Document doc = m.unmarshal(f, Document.class);
				args.setResource(doc);
				SolrIndexerTest.solrIndexer.index(args);
			}
		}
	}

	@Test
	public void indexNoCore() throws UnexpectedException, InvalidParameterException, ServiceNotConfiguredException, WebLabCheckedException {
		String status = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).getStatus();
		Matcher m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {
			final int nbDoc = Integer.parseInt(m.group(1));

			SolrIndexerTest.solrIndexer.getConf().setNoCore(true);

			Document doc = DocumentCreator.createDocWithDeutsch();
			final IndexArgs args = new IndexArgs();
			args.setResource(doc);
			SolrIndexerTest.solrIndexer.index(args);

			doc = DocumentCreator.createDocWithDeutsch();
			args.setResource(doc);
			args.setUsageContext("context1");
			SolrIndexerTest.solrIndexer.index(args);

			SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).commit();
			status = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null).getStatus();

			m = SolrComponentIndexTest.nbDocInCoreStatusPattern.matcher(status);
			if (m.matches()) {
				Assert.assertTrue((nbDoc + 2) <= Integer.parseInt(m.group(1)));
			} else {
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			}
		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}


	@Test
	public void indexUri() throws Exception {
		final String context = null;
		final SolrComponent solrComponent = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, context);

		/*
		 * empty index (to avoid bugs in tests in case of mulitple tests using
		 * the same index)
		 */
		solrComponent.deleteDocbyQuery("*:*");

		final String fieldName = "person_facet";
		/*
		 * search for people field to set URI indexation option
		 */
		Field peopleField = null;
		for (final Field field : SolrIndexerTest.solrIndexer.getConf().getFields()) {
			if (field.getName().equals(fieldName)) {
				peopleField = field;
				break;
			}
		}

		Assert.assertNotNull(peopleField);
		/*
		 * set URI indexing on
		 */
		peopleField.setIndexNonCandidateInstanceAsUri(true);

		final IndexArgs args = new IndexArgs();
		args.setUsageContext(context);
		final String entityLabel = "John Doe";
		final String entityType = "http://weblab.ow2.org/wookie.owl#Person";
		final String entityUri = "weblab:" + UUID.randomUUID();
		final String queryUri = fieldName + ":\"" + entityUri + "\"";
		final String queryLabel = fieldName + ":\"" + entityLabel + "\"";

		args.setResource(SolrIndexerTest.createDocWithSegment(false, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);
		solrComponent.commit();

		/*
		 * one URI and no label indexed
		 */
		this.checkQuery(solrComponent, queryUri, 1);
		this.checkQuery(solrComponent, queryLabel, 0);

		/*
		 * empty index
		 */
		solrComponent.deleteDocbyQuery("*:*");

		args.setResource(SolrIndexerTest.createDocWithSegment(true, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);
		solrComponent.commit();

		/*
		 * one label and no URI indexed
		 */
		this.checkQuery(solrComponent, queryUri, 0);
		this.checkQuery(solrComponent, queryLabel, 1);

		/*
		 * empty index
		 */
		solrComponent.deleteDocbyQuery("*:*");

		/*
		 * set URI indexing off
		 */
		peopleField.setIndexNonCandidateInstanceAsUri(false);
		args.setResource(SolrIndexerTest.createDocWithSegment(true, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);
		args.setResource(SolrIndexerTest.createDocWithSegment(false, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);
		solrComponent.commit();

		/*
		 * no URI added and two labels indexed (because field configuration has
		 * been set to false)
		 */
		this.checkQuery(solrComponent, queryUri, 0);
		this.checkQuery(solrComponent, queryLabel, 2);

	}


	@Test
	public void indexTypeOnly() throws Exception {

		final String context = null;
		final SolrComponent solrComponent = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, context);

		/*
		 * empty index (to avoid bugs in tests in case of mulitple tests using
		 * the same index)
		 */
		solrComponent.deleteDocbyQuery("*:*");

		final String fieldName = "person_facet";
		/*
		 * search for people field to set URI indexation option
		 */
		Field peopleField = null;
		for (final Field field : SolrIndexerTest.solrIndexer.getConf().getFields()) {
			if (field.getName().equals(fieldName)) {
				peopleField = field;
				break;
			}
		}
		/*
		 * set Type indexing on
		 */
		Assert.assertNotNull(peopleField);
		peopleField.setIndexTypeOnly(true);

		final IndexArgs args = new IndexArgs();
		args.setUsageContext(context);
		final String entityLabel = "John Doe";
		final String entityType = "http://xmlns.com/foaf/0.1/Person";
		final String entityUri = "weblab:" + UUID.randomUUID();
		final String queryUri = fieldName + ":\"" + entityUri + "\"";
		final String queryType = fieldName + ":\"" + entityType + "\"";
		final String queryLabel = fieldName + ":\"" + entityLabel + "\"";

		args.setResource(SolrIndexerTest.createDocWithSegment(true, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);

		solrComponent.commit();

		/*
		 * one Type, no label and no uri
		 */
		this.checkQuery(solrComponent, queryType, 1);
		this.checkQuery(solrComponent, queryLabel, 0);
		this.checkQuery(solrComponent, queryUri, 0);

		/*
		 * empty index
		 */
		solrComponent.deleteDocbyQuery("*:*");

		/*
		 * set Type indexing off
		 */
		peopleField.setIndexTypeOnly(false);
		args.setResource(SolrIndexerTest.createDocWithSegment(true, entityUri, entityType, entityLabel));
		SolrIndexerTest.solrIndexer.index(args);
		solrComponent.commit();

		/*
		 * no Type added, still no URI, but one label(because field
		 * configuration has been set to false)
		 */
		this.checkQuery(solrComponent, queryType, 0);
		this.checkQuery(solrComponent, queryUri, 0);
		this.checkQuery(solrComponent, queryLabel, 1);

	}


	/**
	 * Generate a WebLab Document containing a text section with a segment
	 * annotated with the specified information.
	 *
	 * @param isCandidate
	 *            specify if the entity is candidate
	 * @param entityUri
	 *            uri of the entity to be added
	 * @param entityType
	 *            Class of the entity
	 * @param entityLabel
	 *            label of the entity
	 * @return the generated WebLab Document according to the parameters
	 */
	public static Document createDocWithSegment(final boolean isCandidate, final String entityUri, final String entityType, final String entityLabel) {
		final Document document = WebLabResourceFactory.createResource("solr-test", UUID.randomUUID() + "", Document.class);
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		final String startingText = "I'm ";
		text.setContent(startingText + entityLabel);
		final Segment segment = SegmentFactory.createAndLinkLinearSegment(text, startingText.length(), text.getContent().length());
		final WProcessingAnnotator wpa = new WProcessingAnnotator(text);
		final URI entityURI = URI.create(entityUri);
		wpa.startInnerAnnotatorOn(URI.create(segment.getUri()));
		wpa.writeRefersTo(entityURI);
		wpa.endInnerAnnotator();
		wpa.startInnerAnnotatorOn(entityURI);
		wpa.writeLabel(entityLabel);
		wpa.writeType(URI.create(entityType));
		wpa.writeCandidate(Boolean.valueOf(isCandidate));
		return document;
	}


	/**
	 * Assert that a query has the expected number of results.
	 *
	 * @param solrComponent
	 *            component to search on
	 * @param query
	 *            query to test
	 * @param nbOfResults
	 *            excepted number of results
	 */
	private void checkQuery(final SolrComponent solrComponent, final String query, final long nbOfResults) throws WebLabCheckedException {
		final QueryResponse queryResponse = solrComponent.search(query, 0, 20);
		Assert.assertEquals("Must return " + nbOfResults + " result(s).", nbOfResults, queryResponse.getResults().getNumFound());
	}
}
