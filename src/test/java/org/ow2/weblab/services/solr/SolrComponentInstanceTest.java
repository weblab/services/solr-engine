/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr;

import java.rmi.ServerException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrComponentInstanceTest {

	protected final Log logger = LogFactory.getLog(this.getClass());

	private static SolrComponent comp;

	public static SolrConfig CONF;

	// default is true to test on embedded Jetty server
	private static boolean localServer = true;

	static {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			SolrComponentInstanceTest.CONF = fsxac.getBean(SolrConfig.BEAN_NAME, SolrConfig.class);
		}
	}

	@BeforeClass
	public static void start() throws SolrProtoException, ServerException {
		if (SolrComponentInstanceTest.localServer) {
			SolrBackEnd.launchServer();
			String URL = SolrBackEnd.getURL();
			URL = URL.endsWith("/") ? URL : URL + '/';
			SolrComponentInstanceTest.CONF.setZooKeeperEnsemble(URL);
		}
	}

	@AfterClass
	public static void end() throws SolrProtoException {
		if (SolrComponentInstanceTest.localServer) {
			SolrBackEnd.stopServer();
		}
	}

	@Test
	public void getDefaultInstance() {
		SolrComponentInstanceTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
		this.logger.info(SolrComponentInstanceTest.comp.getStatus());

		final SolrComponent compBis = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);

		Assert.assertEquals(SolrComponentInstanceTest.comp, compBis);
	}

	@Test
	public void getInstanceFromContext() {
		SolrComponentInstanceTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, "test");
		this.logger.info(SolrComponentInstanceTest.comp.getStatus());
		Assert.assertNotSame(SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null), SolrComponentInstanceTest.comp);
	}

	@Test
	public void getInstanceFromEmptyContext() {
		SolrComponentInstanceTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, "");
		this.logger.info(SolrComponentInstanceTest.comp.getStatus());

		final SolrComponent compBis = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
		this.logger.info(compBis.getStatus());

		Assert.assertEquals(SolrComponentInstanceTest.comp, compBis);
	}

}
