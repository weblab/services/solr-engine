/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr;

import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.util.index.Field;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrConfigTest {


	@Test
	public void loadConfig() {
		final SolrConfig conf;
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			conf = fsxac.getBean(SolrConfig.BEAN_NAME, SolrConfig.class);
		}

		Assert.assertNotNull(conf);

		Assert.assertEquals("localhost:18888", conf.getZooKeeperEnsemble());
	}


	@Test
	public void loadConfigWithTrimedProblem() {
		final SolrConfig conf;
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4testToBeTrimed.xml")) {
			conf = fsxac.getBean(SolrConfig.BEAN_NAME, SolrConfig.class);
		}

		for (final Entry<String, Field> entry : conf.getPropertyToFieldMap().entrySet()) {
			for (final String property : entry.getValue().getProperties()) {
				Assert.assertFalse(property.contains("\n"));
			}
		}
	}

}
