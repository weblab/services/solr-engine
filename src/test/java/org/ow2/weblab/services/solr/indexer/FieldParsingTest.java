/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.indexer;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.solr.handler.extraction.ExtractionDateUtil;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class FieldParsingTest {

	private String[] dates = {
		"2011-09-09T16:28:08.000+08:00",
		"2011-08-23T15:52:00+0200"
	};

	private String[] doubles ={
		"1234,987",
		"2332,123"
	};

	private static SolrIndexer solrIndexer;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {

			FieldParsingTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);

			FieldParsingTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());

			FieldParsingTest.solrIndexer.init();
		}
	}

	@Test
	public void testDateParsing() throws ParseException{
		Set<String> formats  = new HashSet<>();
		formats.add("yyyy-MM-dd'T'HH:mm:ssX");
		formats.add("yyyy-MM-dd'T'HH:mm:ss.SSSX");

		for(String date : this.dates){
			ExtractionDateUtil.parseDate(date, formats);
		}
	}

	@Test
	public void testDoubleParsing() throws ParseException{
		for(String value : this.doubles){
			FieldParsingTest.solrIndexer.formatDouble(value);
		}
	}

}
