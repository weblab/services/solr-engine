/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SpellSuggestionTest {


	private static SpellSuggestion spellSuggestion;


	private static SolrIndexer solrIndexer;

	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			SpellSuggestionTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			SpellSuggestionTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			SpellSuggestionTest.solrIndexer.init();

			SpellSuggestionTest.spellSuggestion = factory.getBean(SpellSuggestion.BEAN_NAME, SpellSuggestion.class);
			SpellSuggestionTest.spellSuggestion.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			SpellSuggestionTest.spellSuggestion.init();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	@Ignore
	public void notTestReadyYet() {
		System.err.println("Tests on SpellSuggestion are not ready yet.");
	}

	// @Test
	// public void simpleTest() throws Exception {
	// Document doc = DocumentCreator.createDocumentWithAnnotation();
	//
	// IndexArgs iargs = new IndexArgs();
	// iargs.setResource(doc);
	// solrIndexer.index(iargs);
	//
	// SolrComponent.getInstance(solrIndexer.getSolrURL(), null).commit();
	//
	// StringQuery query = ResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
	// query.setRequest("texy");
	// SearchArgs sargs = new SearchArgs();
	// sargs.setQuery(query);
	// ResultSet set = solrSearcher.search(sargs).getResultSet();
	//
	// ProcessArgs pargs = new ProcessArgs();
	// pargs.setResource(set);
	//
	// set = (ResultSet) spellSuggestion.process(pargs).getResource();
	//
	// System.out.println(ResourceUtil.saveToXMLString(set));
	//
	// Assert.assertEquals(2, set.getResource().size());
	// }
	//
	// @Test
	// public void multipleDocTest() throws Exception {
	// for (Document doc : SolrProto.getDocs()) {
	// IndexArgs iargs = new IndexArgs();
	// iargs.setResource(doc);
	// solrIndexer.index(iargs);
	// }
	// SolrComponent.getInstance(solrIndexer.getSolrURL(), null).commit();
	//
	// StringQuery query = ResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
	// query.setRequest("projext");
	// SearchArgs sargs = new SearchArgs();
	// sargs.setQuery(query);
	// ResultSet set = solrSearcher.search(sargs).getResultSet();
	//
	// ProcessArgs pargs = new ProcessArgs();
	// pargs.setResource(set);
	//
	// set = (ResultSet) spellSuggestion.process(pargs).getResource();
	//
	// System.out.println(ResourceUtil.saveToXMLString(set));
	//
	// Assert.assertEquals(2, set.getResource().size());
	// }
}
