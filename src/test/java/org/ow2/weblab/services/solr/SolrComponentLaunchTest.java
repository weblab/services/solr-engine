/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr;

import java.io.File;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrComponentLaunchTest extends SolrBackEnd {

	private static SolrComponent comp;

	private static SolrConfig CONF;

	// default is true to test on embedded Jetty server
	private static boolean localServer = true;


	@BeforeClass
	public static void start() throws Exception {
		System.setProperty(SolrConfig.SOLR_HOME_SYSTEM_PROPERTY, new File("./src/main/resources/solr").getAbsolutePath());

		SolrBackEnd.LOGGER.info("Setting system property [" + SolrConfig.SOLR_HOME_SYSTEM_PROPERTY + "] to ["
				+ System.getProperty(SolrConfig.SOLR_HOME_SYSTEM_PROPERTY) + "]");

		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			SolrComponentLaunchTest.CONF = fsxac.getBean(SolrConfig.BEAN_NAME, SolrConfig.class);
		}

		if (SolrComponentLaunchTest.localServer) {
			SolrBackEnd.launchServer();
			String URL = SolrBackEnd.getURL();
			URL = URL.endsWith("/") ? URL : URL + '/';
			SolrComponentLaunchTest.CONF.setZooKeeperEnsemble(URL);
		}
	}

	@AfterClass
	public static void end() throws SolrProtoException {
		if (SolrComponentLaunchTest.localServer) {
			SolrBackEnd.stopServer();
		}
	}

	@Test
	public void getDefaultInstanceWithSystemProperty() {
		SolrComponentLaunchTest.comp = SolrComponent.getInstance(SolrComponentLaunchTest.CONF, null);
		SolrBackEnd.LOGGER.info(SolrComponentLaunchTest.comp.getStatus());

		final SolrComponent compBis = SolrComponent.getInstance(SolrComponentLaunchTest.CONF, null);

		Assert.assertEquals(SolrComponentLaunchTest.comp, compBis);
	}

}
