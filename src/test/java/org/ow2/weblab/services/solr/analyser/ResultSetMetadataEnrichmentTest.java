/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.helpers.samples.DocumentCreator;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.indexer.SolrIndexer;
import org.ow2.weblab.services.solr.searcher.SolrSearcher;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class ResultSetMetadataEnrichmentTest {


	private static ResultSetMetadataEnrichment enricher;


	private static SolrIndexer solrIndexer;


	private static SolrSearcher solrSearcher;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			ResultSetMetadataEnrichmentTest.solrIndexer = factory.getBean(SolrIndexer.BEAN_NAME, SolrIndexer.class);
			ResultSetMetadataEnrichmentTest.solrIndexer.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			ResultSetMetadataEnrichmentTest.solrIndexer.init();

			ResultSetMetadataEnrichmentTest.enricher = factory.getBean(ResultSetMetadataEnrichment.BEAN_NAME, ResultSetMetadataEnrichment.class);
			ResultSetMetadataEnrichmentTest.enricher.init();

			ResultSetMetadataEnrichmentTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			ResultSetMetadataEnrichmentTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
			ResultSetMetadataEnrichmentTest.solrSearcher.init();
		}
	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	public void simpleTest() throws Exception {
		final Document doc = DocumentCreator.createDocumentWithAnnotation();

		final IndexArgs iargs = new IndexArgs();
		iargs.setResource(doc);
		ResultSetMetadataEnrichmentTest.solrIndexer.index(iargs);

		SolrComponent.getInstance(ResultSetMetadataEnrichmentTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("text");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = ResultSetMetadataEnrichmentTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) ResultSetMetadataEnrichmentTest.enricher.process(pargs).getResource();

		Value<String> isExposedAsValue = new WProcessingAnnotator(URI.create(doc.getUri()), set.getPok()).readExposedAs();
		Value<String> languageValue = new DublinCoreAnnotator(URI.create(doc.getUri()), set.getPok()).readLanguage();

		Assert.assertTrue(languageValue.hasValue());
		Assert.assertEquals("en", languageValue.firstTypedValue());
		Assert.assertTrue(isExposedAsValue.hasValue());
		Assert.assertEquals("http://weblab-project.org/index.php?title=WebLab&oldid=237", isExposedAsValue.firstTypedValue());
	}


	@Test
	public void multipleDocTest() throws Exception {
		final List<String> docsURIs = new LinkedList<>();

		for (final Document doc : SolrBackEnd.getDocs()) {
			final IndexArgs iargs = new IndexArgs();
			iargs.setResource(doc);
			ResultSetMetadataEnrichmentTest.solrIndexer.index(iargs);

			docsURIs.add(doc.getUri());
		}
		SolrComponent.getInstance(ResultSetMetadataEnrichmentTest.solrIndexer.getConf(), null).commit();

		final StringQuery query = WebLabResourceFactory.createResource("test", "query" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("a*");
		final SearchArgs sargs = new SearchArgs();
		sargs.setQuery(query);
		sargs.setOffset(Integer.valueOf(0));
		sargs.setLimit(Integer.valueOf(10));
		ResultSet set = ResultSetMetadataEnrichmentTest.solrSearcher.search(sargs).getResultSet();

		final ProcessArgs pargs = new ProcessArgs();
		pargs.setResource(set);

		set = (ResultSet) ResultSetMetadataEnrichmentTest.enricher.process(pargs).getResource();

		LogFactory.getLog(this.getClass()).debug(ResourceUtil.saveToXMLString(set));

		final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok());
		for (final String hituri : hlpr.getSubjsOnPred(WebLabRetrieval.IS_LINKED_TO)) {
			for (final String docURI : hlpr.getRessOnPredSubj(hituri, WebLabRetrieval.IS_LINKED_TO)) {
				Assert.assertEquals(1, hlpr.getLitsOnPredSubj(docURI, DublinCore.SOURCE_PROPERTY_NAME).size());
			}
		}
	}

}
