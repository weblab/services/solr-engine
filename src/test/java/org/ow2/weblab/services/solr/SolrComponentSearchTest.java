/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr;

import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;

@Ignore
public class SolrComponentSearchTest {


	private static SolrComponent comp;


	@Before
	public void start() throws Exception {
		SolrComponentInstanceTest.start();
		SolrComponentSearchTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
	}


	@After
	public void end() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	public void testSimpleSearch() throws Exception {
		final int nbRes = 2;

		final List<SolrInputDocument> docs = SolrComponentIndexTest.getEbooksAsSolrInputDocument();

		LogFactory.getLog(this.getClass()).debug("Let's start to index !");
		for (final SolrInputDocument doc : docs) {
			SolrComponentSearchTest.comp.addDocument(doc);
		}
		SolrComponentSearchTest.comp.commit();

		final QueryResponse response = SolrComponentSearchTest.comp.search("a*", 0, nbRes);
		Assert.assertEquals(nbRes, response.getResults().size());

	}

}
