/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;

@Ignore
public class SolrComponentDeleteTest {


	private static final Pattern nbDocInCoreStatusPattern = Pattern.compile(".*numDocs=(\\d*),.*");


	private final Log logger = LogFactory.getLog(SolrComponentInstanceTest.class);


	private static SolrComponent comp;


	@BeforeClass
	public static void start() throws Exception {
		SolrComponentInstanceTest.start();
		SolrComponentDeleteTest.comp = SolrComponent.getInstance(SolrComponentInstanceTest.CONF, null);
	}


	@AfterClass
	public static void end() throws SolrProtoException {
		SolrComponentDeleteTest.comp.commit();
		SolrComponentInstanceTest.end();
	}


	@Test
	public void deleteOneDoc() throws IOException, WebLabCheckedException {
		String status = SolrComponentDeleteTest.comp.getStatus();
		Matcher m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {

			// index several docs...
			int nbDoc = Integer.parseInt(m.group(1));

			final List<SolrInputDocument> docs = SolrComponentIndexTest.getEbooksAsSolrInputDocument();

			this.logger.info("Let's start to index !");
			for (final SolrInputDocument doc : docs) {
				SolrComponentDeleteTest.comp.addDocument(doc);
			}

			SolrComponentDeleteTest.comp.commit();

			status = SolrComponentDeleteTest.comp.getStatus();
			this.logger.info(status);

			m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
			if (!m.matches()) {
				throw new WebLabCheckedException("Cannot read Solr core status...");
			}

			nbDoc = Integer.parseInt(m.group(1));

			// several docs indexed, try to remove one.

			SolrComponentDeleteTest.comp.deleteDocbyURI(docs.get(0).getFieldValue(SolrConfig.FIELD_ID).toString());

			SolrComponentDeleteTest.comp.commit();

			status = SolrComponentDeleteTest.comp.getStatus();
			this.logger.info(status);

			m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
			if (m.matches()) {
				Assert.assertEquals(nbDoc - 1, Integer.parseInt(m.group(1)));
			} else {
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			}

		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}


	@Test
	public void deleteDocByQuery() throws IOException, WebLabCheckedException {
		String status = SolrComponentDeleteTest.comp.getStatus();
		Matcher m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
		if (m.matches()) {

			// index several docs...
			int nbDoc = Integer.parseInt(m.group(1));

			final List<SolrInputDocument> docs = SolrComponentIndexTest.getEbooksAsSolrInputDocument();

			this.logger.info("Let's start to index !");
			for (final SolrInputDocument doc : docs) {
				SolrComponentDeleteTest.comp.addDocument(doc);
			}

			SolrComponentDeleteTest.comp.commit();

			status = SolrComponentDeleteTest.comp.getStatus();
			this.logger.info(status);

			m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
			if (!m.matches()) {
				throw new WebLabCheckedException("Cannot read Solr core status...");
			}
			nbDoc = Integer.parseInt(m.group(1));

			SolrComponentDeleteTest.comp.deleteDocbyQuery("a*");
			SolrComponentDeleteTest.comp.commit();

			status = SolrComponentDeleteTest.comp.getStatus();
			this.logger.info(status);

			m = SolrComponentDeleteTest.nbDocInCoreStatusPattern.matcher(status);
			if (m.matches()) {
				Assert.assertTrue(nbDoc > Integer.parseInt(m.group(1)));
			} else {
				Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
			}

		} else {
			Assert.fail("Cannot read nb indexed docs in status [" + status + "].");
		}
	}

}
