/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.searcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.services.proto.SolrBackEnd;
import org.ow2.weblab.services.proto.SolrBackEnd.SolrProtoException;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrComponentInstanceTest;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.services.solr.indexer.SolrIndexerTest;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Ignore
public class SolrSearchTest {


	private static SolrSearcher solrSearcher;


	private static SimpleDateFormat formater = new SimpleDateFormat(SolrConfig.SOLR_DATE_FORMAT);


	private static boolean verbose = false;


	@BeforeClass
	public static void setUp() throws Exception {
		final SolrIndexerTest indexTest = new SolrIndexerTest();
		SolrIndexerTest.start();
		indexTest.indexSomeDocuments();

		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {

			SolrSearchTest.solrSearcher = factory.getBean(SolrSearcher.BEAN_NAME, SolrSearcher.class);
			SolrSearchTest.solrSearcher.getConf().setZooKeeperEnsemble(SolrBackEnd.getURL());
	//		SolrSearchTest.solrSearcher.getConf().setSolrHome(new File(SolrSearchTest.solrSearcher.getConf().getSolrHome()).getAbsoluteFile().getAbsolutePath());
	//		SolrSearchTest.solrSearcher.getConf().setSolrData(new File(SolrSearchTest.solrSearcher.getConf().getSolrData()).getAbsoluteFile().getAbsolutePath());
			SolrSearchTest.solrSearcher.init();
		}

		SolrComponent.getInstance(SolrSearchTest.solrSearcher.getConf(), null).commit();
	}


	@AfterClass
	public static void deleteTestingIndex() throws SolrProtoException {
		SolrComponentInstanceTest.end();
	}


	@Test
	public void testSimpleSearch() throws Exception {
		final SearchArgs args = new SearchArgs();
		final StringQuery q0 = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q0.setRequest("*:*");

		args.setQuery(q0);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		final SearchReturn res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);

		final ResultSet set = res.getResultSet();
		Assert.assertNotNull(set);

		// exploring result set
		final WRetrievalAnnotator wra = new WRetrievalAnnotator(new URI(set.getUri()), set.getPok());
		// getting the total number of result in the ResultSet
		final Value<Integer> nbOfRes = wra.readNumberOfResults();
		if (nbOfRes != null) {
			final int numberOfHits = nbOfRes.firstTypedValue().intValue();
			if (numberOfHits > 0) {

				// getting hit and link to docURI
				final Value<URI> hits = wra.readHit();
				// do what you want with the list of hit
				// ...
				if (hits.size() > 0) {
					for (final URI hit : hits) {
						wra.startInnerAnnotatorOn(hit);
						final URI docURI = wra.readLinkedTo().firstTypedValue();
						// do what you want with docURI
						// ...
						Assert.assertNotNull(docURI);
						wra.endInnerAnnotator();
					}
				}
			}
		}
		this.verbousOutPut(q0, res);
	}


	@Test
	public void testSearchWithSpecialOrdering() throws Exception {
		final SearchArgs args = new SearchArgs();
		final StringQuery q = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q.setRequest("a*");

		args.setQuery(q);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		SearchReturn res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);

		this.verbousOutPut(q, res);

		WRetrievalAnnotator wra = new WRetrievalAnnotator(res.getResultSet());
		Value<URI> hits = wra.readHit();
		int min = Integer.MAX_VALUE;
		URI firstHit = null;
		if (hits != null) {
			for (final URI hit : hits) {
				wra.startInnerAnnotatorOn(hit);
				if (wra.readRank().firstTypedValue().intValue() < min) {
					firstHit = hit;
				}
			}
		}

		wra.writeToBeRankedBy(URI.create(WebLabProcessing.HAS_GATHERING_DATE));
		wra.writeToBeRankedAscending(Boolean.TRUE);

		res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);

		if (firstHit != null) {
			wra = new WRetrievalAnnotator(res.getResultSet());
			hits = wra.readHit();
			min = Integer.MAX_VALUE;
			URI newFirstHit = null;
			for (final URI hit : hits) {
				wra.startInnerAnnotatorOn(hit);
				if (wra.readRank().firstTypedValue().intValue() < min) {
					newFirstHit = hit;
				}
			}
			Assert.assertNotSame(firstHit, newFirstHit);
		}

		this.verbousOutPut(q, res);
	}


	@Test
	public void testSimSearch() throws Exception {
		final SearchArgs args = new SearchArgs();

		final File folder = new File(SolrIndexerTest.SAMPLES_TO_BE_INDEXED);
		final WebLabMarshaller m = new WebLabMarshaller();
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));
		for (final File f : folder.listFiles()) {
			if (f.getName().endsWith(".xml")) {
				final Document doc = m.unmarshal(f, Document.class);
				final SimilarityQuery q = WebLabResourceFactory.createResource("test", "query41", SimilarityQuery.class);
				q.getResource().add(doc);
				args.setQuery(q);

				final SearchReturn res = SolrSearchTest.solrSearcher.search(args);
				Assert.assertNotNull(res);

				this.verbousOutPut(q, res);
			}
		}
	}


	@Test
	public void testSearchWithScope() throws Exception {
		// launch simple query : should have few results
		final SearchArgs args = new SearchArgs();
		final StringQuery q = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q.setRequest("a*");

		args.setQuery(q);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		SearchReturn res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);
		ResultSet rSet = res.getResultSet();
		Assert.assertNotNull(rSet);

		this.verbousOutPut(q, res);

		WRetrievalAnnotator wra = new WRetrievalAnnotator(new URI(rSet.getUri()), rSet.getPok());
		final int nbResOrigins = wra.readNumberOfResults().firstTypedValue().intValue();

		// add scope on query : should reduce the number of results
		new WRetrievalAnnotator(q).writeScope(URI.create(DublinCore.SOURCE_PROPERTY_NAME));

		res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);
		rSet = res.getResultSet();
		Assert.assertNotNull(rSet);

		this.verbousOutPut(q, res);

		wra = new WRetrievalAnnotator(new URI(rSet.getUri()), rSet.getPok());
		final int nbRes = wra.readNumberOfResults().firstTypedValue().intValue();

		Assert.assertTrue(nbResOrigins > nbRes);
	}


	@Test
	public void testComposedSearch() throws Exception {
		// launch simple query : should have no results
		final SearchArgs args = new SearchArgs();
		final StringQuery q0 = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q0.setRequest("a*");

		args.setQuery(q0);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		SearchReturn res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);
		ResultSet rSet = res.getResultSet();
		Assert.assertNotNull(rSet);

		this.verbousOutPut(q0, res);

		WRetrievalAnnotator wra = new WRetrievalAnnotator(new URI(rSet.getUri()), rSet.getPok());
		final int nbResOrigins = wra.readNumberOfResults().firstTypedValue().intValue();

		LogFactory.getLog(this.getClass()).debug(String.valueOf(nbResOrigins));

		// build ComposedQuery
		final StringQuery q1 = WebLabResourceFactory.createResource("test", "query1", StringQuery.class);
		q1.setRequest("matin");
		final ComposedQuery q = WebLabResourceFactory.createResource("test", "cquery", ComposedQuery.class);
		q.setOperator(Operator.AND);
		q.getQuery().add(q0);
		q.getQuery().add(q1);
		args.setQuery(q);

		res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);
		rSet = res.getResultSet();
		Assert.assertNotNull(rSet);

		this.verbousOutPut(q, res);

		wra = new WRetrievalAnnotator(new URI(rSet.getUri()), rSet.getPok());
		final int nbRes = wra.readNumberOfResults().firstTypedValue().intValue();

		Assert.assertTrue(nbRes < nbResOrigins);

		final Calendar cal = Calendar.getInstance();
		cal.set(2011, 7, 15, 15, 50, 00);

		// build complex ComposedQuery
		final StringQuery q2 = WebLabResourceFactory.createResource("test", "query2", StringQuery.class);
		q2.setRequest("[" + SolrSearchTest.formater.format(cal.getTime()) + " " + SolrSearchTest.formater.format(new Date()) + "]");

		new WRetrievalAnnotator(q2).writeScope(URI.create(WebLabProcessing.HAS_GATHERING_DATE));

		final ComposedQuery q3 = WebLabResourceFactory.createResource("test", "cquery3", ComposedQuery.class);
		q3.setOperator(Operator.AND);
		q3.getQuery().add(q2);
		q3.getQuery().add(q);

		args.setQuery(q3);

		res = SolrSearchTest.solrSearcher.search(args);
		Assert.assertNotNull(res);
		rSet = res.getResultSet();
		Assert.assertNotNull(rSet);

		this.verbousOutPut(q, res);

		wra = new WRetrievalAnnotator(new URI(rSet.getUri()), rSet.getPok());
		final int nbResFinal = wra.readNumberOfResults().firstTypedValue().intValue();

		Assert.assertEquals(3, nbResFinal);
	}


	@Test
	public void testSearchPerf() throws Exception {
		final File propsFile = new File("src/test/resources/queries_gen.txt");
		final Properties props = new Properties();
		try (FileInputStream stream = new FileInputStream(propsFile)) {
			props.load(stream);
		}

		final SearchArgs args = new SearchArgs();
		final StringQuery q = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);

		args.setQuery(q);
		args.setOffset(Integer.valueOf(0));
		args.setLimit(Integer.valueOf(10));

		long totalTime = 0;
		final int max = 100;
		for (int i = 0; i < max; i++) {
			q.setRequest(props.getProperty("q" + i));

			final long startTime = System.currentTimeMillis();

			SolrSearchTest.solrSearcher.search(args);

			final long finalTime = System.currentTimeMillis();

			totalTime += finalTime - startTime;
		}
		LogFactory.getLog(this.getClass()).debug("average time : " + (totalTime / max) + "ms");
	}


	@Test
	public void testSearchInThread() throws Exception {
		final int nbThread = 3;

		final List<Callable<SolrSearchThread>> get = new ArrayList<>();

		for (int i = 0; i < nbThread; i++) {
			get.add(new SolrSearchThread(SolrSearchTest.solrSearcher, nbThread, 50));
		}
		final ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		final List<Future<SolrSearchThread>> results = executor.invokeAll(get, 5, TimeUnit.MINUTES);
		long metaDuration = 0;
		for (final Future<SolrSearchThread> future : results) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			metaDuration += future.get().getTotalTime();
		}
		LogFactory.getLog(this.getClass()).debug("Threads executed correctly.");
		LogFactory.getLog(this.getClass()).debug("Mean get time over " + nbThread + " thread: " + (metaDuration / nbThread) + " ms");
	}


	protected void verbousOutPut(final Query q, final SearchReturn res) throws WebLabCheckedException, FileNotFoundException {
		if (SolrSearchTest.verbose) {
			final String requestXml = ResourceUtil.saveToXMLString(q);
			try (final PrintWriter requestFile = new PrintWriter(new File("target/request.xml"))) {
				requestFile.write(requestXml);
			}
			final String resultXml = ResourceUtil.saveToXMLString(res.getResultSet());
			LogFactory.getLog(this.getClass()).debug("Results : \n" + resultXml);
			try (final PrintWriter resultFile = new PrintWriter(new File("target/result.xml"))) {
				resultFile.write(resultXml);
			}
		}
	}

}
