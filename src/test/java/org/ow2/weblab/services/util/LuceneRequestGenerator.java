/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.util;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

enum Operator {
	AND, OR
}

class Tree {


	Tree leftTree;


	Tree rightTree;


	Operator nodeOperator;


	Operator wordOperator;


	List<String> words;


	public Tree(final Tree leftTree, final Tree rightTree, final Operator nodeOperator, final List<String> words, final Operator wordOperator) {
		this.leftTree = leftTree;
		this.rightTree = rightTree;
		this.nodeOperator = nodeOperator;
		this.words = words;
		this.wordOperator = wordOperator;
	}


	public Tree getLeftTree() {
		return this.leftTree;
	}


	public Operator getNodeOperator() {
		return this.nodeOperator;
	}


	public Tree getRightTree() {
		return this.rightTree;
	}


	public List<String> getWords() {
		return this.words;
	}


	public Operator getWordOperator() {
		return this.wordOperator;
	}


	public void setLeftTree(final Tree leftTree) {
		this.leftTree = leftTree;
	}


	public void setNodeOperator(final Operator nodeOperator) {
		this.nodeOperator = nodeOperator;
	}


	public void setRightTree(final Tree rightTree) {
		this.rightTree = rightTree;
	}
}

public class LuceneRequestGenerator {


	static void ParcoursInfixe(final Tree t, final Writer writer) throws IOException {
		if (t == null) {
			throw new NullPointerException("T is NULL !!!!!");
		}

		if ((t.getRightTree() != null) || (t.getLeftTree() != null) || (t.getWords().size() > 1)) {
			writer.write("(");
		}

		if (t.getLeftTree() != null) {
			LuceneRequestGenerator.ParcoursInfixe(t.getLeftTree(), writer);
		}
		for (int i = 0; i < t.getWords().size(); i++) {
			if (i > 0) {
				writer.write(" " + t.getWordOperator().toString() + " ");
			}

			writer.write(t.getWords().get(i));
		}

		if (t.getRightTree() != null) {
			writer.write(" " + t.getNodeOperator() + " ");
			LuceneRequestGenerator.ParcoursInfixe(t.getRightTree(), writer);
		}

		if ((t.getRightTree() != null) || (t.getLeftTree() != null) || (t.getWords().size() > 1)) {
			writer.write(")");
		}

	}


	static List<String> addAll(final String... strList) {
		final List<String> l = new ArrayList<>();
		Collections.addAll(l, strList);

		return l;
	}


	static List<String> getWordsList(final List<String> words, final int randomLimit) {
		final Random rand = new Random();
		int nwords = rand.nextInt(randomLimit) + 1;

		if (nwords > words.size()) {
			nwords = words.size();
		}

		final List<String> subWords = new ArrayList<>(words.subList(0, nwords));
		words.removeAll(subWords);

		return subWords;
	}


	static Operator generateRandomOperator() {
		final Random rand = new Random();
		final boolean bool = rand.nextBoolean();

		return bool ? Operator.AND : Operator.OR;
	}


	static List<String> getAllWords(final File file, final int wordCount) throws IOException {
		final List<String> words = new ArrayList<>();

		try (final FileInputStream fstream = new FileInputStream(file);
				final DataInputStream in = new DataInputStream(fstream);
				final BufferedReader br = new BufferedReader(new InputStreamReader(in));) {
			String word;
			while ((word = br.readLine()) != null) {
				if ((word.length() > 0) && StringUtils.isAlpha(word) && StringUtils.isAsciiPrintable(word)) {
					words.add(word);
				}
			}
			in.close();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		}

		Collections.shuffle(words);

		return words.subList(0, Math.min(wordCount, words.size()));
	}


	static String generateLuceneRequest(final List<String> allWordsInit, final boolean onlyWithOrOperator) throws IOException {
		final StringWriter writer = new StringWriter();

		final int randomLimit = 15;

		for (int i = 0; i < 1; i++) {
			final List<String> allWords = new ArrayList<>(allWordsInit);
			Collections.shuffle(allWords);
			Tree rightTree = null;

			while (allWords.size() > 0) {
				Operator operator;

				if (!onlyWithOrOperator) {
					operator = LuceneRequestGenerator.generateRandomOperator();
				} else {
					operator = Operator.OR;
				}

				if (rightTree == null) {
					rightTree = new Tree(null, null, null, LuceneRequestGenerator.getWordsList(allWords, randomLimit), operator);
				}

				if (!onlyWithOrOperator) {
					operator = LuceneRequestGenerator.generateRandomOperator();
				}

				rightTree = new Tree(null, rightTree, operator, LuceneRequestGenerator.getWordsList(allWords, randomLimit), operator);
			}

			final Tree root = rightTree;
			LuceneRequestGenerator.ParcoursInfixe(root, writer);
		}

		return writer.toString();
	}


	public static void main(final String[] args) throws Exception {
		final File wordListFile = new File("src/test/resources/wordList.txt");
		final List<String> allWordsInit = LuceneRequestGenerator.getAllWords(wordListFile, 500);
		int requestIndex = 0;
		try (final PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/test/resources/queries_gen.txt"), "UTF-8"))) {

			for (int i = 0; i < 250; i++) {
				final String luceneRequest = LuceneRequestGenerator.generateLuceneRequest(allWordsInit, false);
				writer.println("q" + requestIndex++ + "=" + luceneRequest);
			}

			for (int i = 0; i < 250; i++) {
				final String luceneRequest = LuceneRequestGenerator.generateLuceneRequest(allWordsInit, true);
				writer.println("q" + requestIndex++ + "=" + luceneRequest);
			}
		}
	}


	public static void _main(final String[] args) throws Exception {
		final File wordListFile = new File("src/test/resources/wordList.txt");
		final List<String> allWordsInit = LuceneRequestGenerator.getAllWords(wordListFile, 25000);
		int requestIndex = 0;
		try (final PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream("src/test/resources/queries_gen.txt"), "UTF-8"))) {

			for (int i = 0; i < allWordsInit.size(); i++) {
				writer.println("q" + requestIndex++ + "=" + allWordsInit.get(i));
			}

		}
	}
}
