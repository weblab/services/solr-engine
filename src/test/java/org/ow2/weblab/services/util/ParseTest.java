/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.util;

import java.text.ParseException;
import java.util.TimeZone;

import org.apache.commons.logging.LogFactory;
import org.apache.solr.handler.extraction.ExtractionDateUtil;
import org.apache.solr.util.DateMathParser;
import org.junit.Test;

public class ParseTest {


	@Test
	public void parseDate() throws ParseException {
		final String[] samples = { "2011-08-23T18:29:58+0200", "2011-08-25T16:07:37+02:00" };
		for (final String value : samples) {
			ExtractionDateUtil.parseDate(value);
		}
	}


	@Test
	public void testDateMathParser() throws ParseException {
		final TimeZone tz = TimeZone.getDefault();

		final DateMathParser mparser = new DateMathParser(tz);

		LogFactory.getLog(this.getClass()).debug((mparser.parseMath("+0DAY")));
		LogFactory.getLog(this.getClass()).debug((mparser.parseMath("-300DAY")));
	}

}
