/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.util;

import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.SolrQueryParser;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public final class SolrQueryParserTest {

	private static Log log = LogFactory.getLog(SolrQueryParserTest.class);
	private static SolrConfig conf;


	@BeforeClass
	public static void setUp() throws Exception {
		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("src/test/resources/cxf-servlet4test.xml")) {
			SolrQueryParserTest.conf = factory.getBean(SolrConfig.BEAN_NAME, SolrConfig.class);
		}
	}


	@Test
	public void testQueryWithEntityTypeScope() throws WebLabCheckedException {
		final StringQuery q = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q.setRequest("a*");

		// add scope on query : should reduce the number of results
		new WRetrievalAnnotator(q).writeScope(URI.create("http://weblab.ow2.org/wookie.owl#Place"));

		final SolrQueryParser parser = new SolrQueryParser(SolrQueryParserTest.conf);

		final String request = parser.getRequestWithScope(q, new JenaResourceHelper(q));

		log.debug("Request as string is [" + request + "].");

		Assert.assertTrue(request.contains(":"));
	}

	@Test
	public void testANDOR() throws WebLabCheckedException{

		final StringQuery q0 = WebLabResourceFactory.createResource("test", "query0", StringQuery.class);
		q0.setRequest("a OR b");

		final StringQuery q1 = WebLabResourceFactory.createResource("test", "query1", StringQuery.class);
		q1.setRequest("c");

		final ComposedQuery q2 = WebLabResourceFactory.createResource("test", "query2", ComposedQuery.class);
		q2.getQuery().add(q0);
		q2.getQuery().add(q1);

		final SolrQueryParser parser = new SolrQueryParser(SolrQueryParserTest.conf);
		final String request = parser.getRequestWithScope(q2, new JenaResourceHelper(q2));

		log.debug("Request as string is [" + request + "].");

		Assert.assertTrue("((a OR b) AND (c))".compareTo(request) == 0);
	}

}
