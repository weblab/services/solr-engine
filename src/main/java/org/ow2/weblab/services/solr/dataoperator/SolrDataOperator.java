/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.dataoperator;

import java.util.List;

import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Cleanable;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.cleanable.CleanArgs;
import org.ow2.weblab.core.services.cleanable.CleanReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Cleanable")
public class SolrDataOperator implements Cleanable {


	public static final String BEAN_NAME = "dataoperatorServiceBean";


	protected Log logger = LogFactory.getLog(this.getClass());


	protected SolrConfig conf;


	protected boolean dropAllOnEmptyTrashList;


//	@PostConstruct
//	public void init() {
//		try {// init default instance
//			SolrComponent.getInstance(this.conf, null);
//			new URL(this.conf.getSolrURL());
//		} catch (final WebLabCheckedException e) {
//			throw new WebLabUncheckedException("Cannot start the SolrComponent.", e);
//		} catch (final MalformedURLException e) {
//			throw new WebLabUncheckedException("Cannot start the service. The solrULR is invalid [" + this.conf.getSolrURL() + "].", e);
//		}
//		this.logger.debug("SolrDataOperator service started.");
//	}


	@PreDestroy
	public void destroy() {
		// nothing to do I guess
		this.logger.debug("SolrDataOperator service destroyed.");
	}


	@Override
	public CleanReturn clean(final CleanArgs args) throws ServiceNotConfiguredException, InsufficientResourcesException, UnexpectedException,
			InvalidParameterException, AccessDeniedException, ContentNotAvailableException, UnsupportedRequestException {

		if (args == null) {
			final String message = "CleanArgs was null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		
		this.logger.debug("'<"+Constants.SERVICE_NAME+" : Cleanable >'" + " Start processing sets of document");

		final List<String> uris = args.getTrash();
		
		
		String uc;
		if (args.getUsageContext() != null){
			uc = args.getUsageContext();
		} else {
			uc = this.conf.getCollectionContext();
		}
		if (uris.isEmpty() && this.dropAllOnEmptyTrashList) {
			this.logger.trace("Clean method called with an empty trash list; dropAllOnEmptyTrashList is true --> drop all from context: '" + uc + "'.");
			this.deleteAllDoc(uc);
		} else if (uris.isEmpty()) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Clean method called with an empty trash list; dropAllOnEmptyTrashList is false --> Nothing will be done. ");
		} else {
			this.logger.trace("Clean method called to delete URIs: " + uris + ". On context '" + uc + "'.");
			for (final String uri : uris) {
				this.deleteByDocURI(uc, uri);
			}
		}
		this.logger.debug("'<"+Constants.SERVICE_NAME+" : Cleanable >'" + " End processing sets of document");
		return new CleanReturn();
	}


	protected boolean deleteByDocURI(final String usageContext, final String uri) throws UnsupportedRequestException {
		final StringBuffer query = new StringBuffer(SolrConfig.FIELD_ID);
		query.append(':');
		query.append('"');
		query.append(uri);
		query.append('"');
		try {
			this.doDelete(usageContext, query.toString());
		} catch (final WebLabCheckedException e) {
			final String message = "Cannot achieve the request for deletion: " + e.getMessage();
			throw ExceptionFactory.createUnsupportedRequestException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message, e);
		}
		return true;
	}



	protected boolean deleteAllDoc(final String usageContext) throws UnsupportedRequestException {
		try {
			this.doDelete(usageContext, "*:*");
		} catch (final WebLabCheckedException e) {
			final String message = "Cannot achieve the request for deletion: " + e.getMessage();
			throw  ExceptionFactory.createUnsupportedRequestException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message, e);
		}
		return true;
	}


	protected void doDelete(final String context, final String query) throws WebLabCheckedException {
		final SolrComponent instance = SolrComponent.getInstance(this.conf, context);
		instance.deleteDocbyQuery(query);
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}


	/**
	 * @return the dropAllOnEmptyTrashList
	 */
	public boolean isDropAllOnEmptyTrashList() {
		return this.dropAllOnEmptyTrashList;
	}


	/**
	 * @param dropAllOnEmptyTrashList
	 *            the dropAllOnEmptyTrashList to set
	 */
	public void setDropAllOnEmptyTrashList(final boolean dropAllOnEmptyTrashList) {
		this.dropAllOnEmptyTrashList = dropAllOnEmptyTrashList;
	}

}
