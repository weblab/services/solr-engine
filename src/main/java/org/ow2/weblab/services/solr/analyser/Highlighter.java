/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.SolrQueryParser;
import org.ow2.weblab.util.WebLabQueryParser;
import org.ow2.weblab.util.constants.Constants;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class Highlighter implements Analyser {

	private static final int DEFAULT_SNIPPET_SIZE = 300;

	public static final String BEAN_NAME = "highlighterServiceBean";

	private Log logger;

	private SolrConfig conf;

	private WebLabQueryParser parser;

	@PostConstruct
	public void init() {
		this.logger = LogFactory.getLog(Highlighter.class);
		this.parser = new SolrQueryParser(this.conf);
	}

	@PreDestroy
	public void destroy() {
		this.logger.info("'<"+Constants.SERVICE_NAME+">'"+" Destroying SolR Highlighter service.");
		// nothing to do I guess
	}

	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException,
			ContentNotAvailableException, InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {
		this.checkArgs(args);

		final ResultSet enrichSet = this.highLightHitInResultSet(args.getUsageContext(), (ResultSet) args.getResource());

		final ProcessReturn ret = new ProcessReturn();

		ret.setResource(enrichSet);
		return ret;
	}

	public ResultSet highLightHitInResultSet(final String usageContext, final ResultSet set) {

		try {
			if (!(set.getResource().get(0) instanceof Query)) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There is no query in the ResultSet.");
			}
			final Query query = (Query) set.getResource().get(0);
			final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(set.getUri()), set.getPok());

			if (wra.readNumberOfResults().firstTypedValue().intValue() == 0) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There are no results in the ResultSet.");
			}

			final int offset = wra.readExpectedOffset().firstTypedValue().intValue();
			final int limit = wra.readHit().size();

			SolrComponent instance;
			if (this.conf.isNoCore()) {
				instance = SolrComponent.getInstance(this.conf, null);
			} else {
				instance = SolrComponent.getInstance(this.conf, usageContext);
			}

			// check if the query has annotation to expect a specific behavior
			boolean ascOrder = false;
			String orderedByProperty = null;
			if (query.getAnnotation().size() > 0) {
				final ResourceHelper hlpr = new JenaResourceHelper(query);
				ascOrder = this.parser.getOrder(query, hlpr);
				orderedByProperty = this.parser.getOrderBy(query, hlpr);
			}

			final QueryResponse response = instance.highlight(this.parser.getRequest(query, new JenaResourceHelper(query)), offset, limit, orderedByProperty,
					ascOrder ? ORDER.asc : ORDER.desc);

			final Map<String, Map<String, List<String>>> highlights = response.getHighlighting();

			if(highlights == null){
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"No Highlighting information in SolR response.");
			}

			final JenaPoKHelper hlpr = new JenaPoKHelper(set.getPok(), false);
			for (final SolrDocument hit : response.getResults()) {
				final String resourceUri = String.valueOf(hit.getFieldValue(SolrConfig.FIELD_ID));

				final Set<String> hits = hlpr.getSubjsOnPredRes(WebLabRetrieval.IS_LINKED_TO, resourceUri);
				if (hits.size() != 1) {
					this.logger.trace("Well, the resultSet is not clear: multiple hits (or none) are linked to [" + resourceUri
							+ "]. Let's try to cope with that...");
				}
				for (final String hitUri : hits) {
					Map<String, List<String>> highlightsMap = highlights.get(hit.getFieldValue(SolrConfig.FIELD_ID));
					this.logger.trace("highlighs map:" + highlightsMap.toString());
					if ( (highlightsMap != null) && (!highlightsMap.isEmpty()) && (highlightsMap.get(SolrConfig.FIELD_TEXT) != null)) {
						final List<String> hitHighlightingList = highlightsMap.get(SolrConfig.FIELD_TEXT);

						final StringBuffer snippet = new StringBuffer();
						for (final String hl : hitHighlightingList) {
							snippet.append(hl);
						}
						hlpr.createLitStat(hitUri, WebLabRetrieval.HAS_DESCRIPTION, snippet.toString());
					} else {
						this.logger.trace("No highlighted content available for hit [" + hitUri + "] on document [" + resourceUri + "]. Trying to use the "
								+ Highlighter.DEFAULT_SNIPPET_SIZE + " first characters of content as snippet.");
						if (hit.getFieldNames().contains(SolrConfig.FIELD_TEXT)) {

							String content = hit.get(SolrConfig.FIELD_TEXT).toString();
							content = content.substring(0, Math.min(content.length(), Highlighter.DEFAULT_SNIPPET_SIZE)).trim() + "...";
							hlpr.createLitStat(hitUri, WebLabRetrieval.HAS_DESCRIPTION, content);
						} else {
							this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"No highlighted content for hit [" + hitUri + "] on document [" + resourceUri
									+ "] and text not available... we can't do anything!");
						}
					}
				}
			}
			hlpr.commit();
			this.logger.trace("Highlighting done for [" + set.getUri() + "].");

			return set;
		} catch (final WebLabCheckedException e) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Cannot highlight results: " + e.getMessage(), e);
		}
		return set;
	}

	private void checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "Input args for [" + this.getClass().getSimpleName() + "] cannot be null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		// TODO finish it...
	}

	public SolrConfig getConf() {
		return this.conf;
	}

	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}

}