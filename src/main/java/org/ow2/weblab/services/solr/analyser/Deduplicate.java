/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.util.List;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * Duplicate detection using SolR source field: same URL =&gt; match.
 * This simple approach is only adapted to local indexing of document (ie static document never updated).
 * This service should be enhanced later on with addtional strategies for duplicate detection.
 *
 * @author Jérémie Doucy
 *
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class Deduplicate implements Analyser {

	private final Log logger = LogFactory.getLog(Deduplicate.class);
	public static final String BEAN_NAME = "deduplicateServiceBean";


	private SolrConfig conf;

	private String sourceField;

	public Deduplicate(SolrConfig solrConfig, String sourceField) {
		super();
		this.conf = solrConfig;
		this.sourceField = sourceField;
	}

	@Override
	public ProcessReturn process(ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException, ContentNotAvailableException,
			InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {

		ProcessReturn processReturn = new ProcessReturn();
		Document document = this.checkArgs(args);
		processReturn.setResource(document);
		/*
		 * getting the dc:source property
		 */
		DublinCoreAnnotator dublinCoreAnnotator = new DublinCoreAnnotator(document);
		List<String> listSources = dublinCoreAnnotator.readSource().getValues();

		if (listSources.size() == 0) {
			final String message = "Document must contains source property: " + DublinCore.SOURCE_PROPERTY_NAME;
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		String source = null;
		if (listSources.size() > 1) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"Multiple source property: " + DublinCore.SOURCE_PROPERTY_NAME + " -> " + listSources);
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"Multiple source property values: ");
			source = "";
			for (String cur : listSources) {
				this.logger.warn(cur);
				source += cur;
			}
		} else {
			source = listSources.get(0);
			this.logger.trace("Trying to diff source: " + source);
		}
		try {
			SolrComponent component = SolrComponent.getInstance(this.conf, "");
			long nbFound = component.search(this.sourceField + ":(\"" + source + "\")", 0, 1).getResults().getNumFound();
			if (nbFound > 0) {
				/*
				 * source already in the index
				 */
				new WProcessingAnnotator(document).writeCanBeIgnored(Boolean.TRUE);
				this.logger.trace("Already in index: " + source);
			} else {
				this.logger.trace("First time for: " + source);
			}
		} catch (WebLabCheckedException e) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"Unable to deduplicate" ,e);
		}

		return processReturn;
	}

	/**
	 * @param args
	 *            The <code>ProcessArgs</code> to check in the begin of
	 *            <code>process</code>.
	 * @return The contained <code>Document</code>
	 * @throws InvalidParameterException
	 *             if we are unable to extract the contained
	 *             <code>Document</code>
	 */
	private Document checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		if (args.getResource() == null) {
			final String message = "Args must contain a non-null Resource to index";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		if (!(args.getResource() instanceof Document)) {
			final String message = "Args must contain at a Document";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}

		return (Document) args.getResource();
	}
}
