/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.util.index.Field;

public class SolrConfig {


	/**
	 * Default query handler defined in solrconfig.xml This one should only
	 * enable to get list of results URI and scores.
	 */
	protected static final String DEFAULT_QUERY_HANDLER_MINIMAL = "weblab";


	/**
	 * Query handler that retrieve result metadata
	 */
	protected static final String DEFAULT_QUERY_HANDLER_META = "weblab_meta";


	/**
	 * Query handler that retrieve allow document highlighting
	 */
	protected static final String DEFAULT_QUERY_HANDLER_HIGHLIGHT = "weblab_highlight";


	/**
	 * Query handler that retrieve facet suggestion
	 */
	protected static final String DEFAULT_QUERY_HANDLER_FACET = "weblab_facet";


	/**
	 * Query handler that retrieve spell suggestion
	 */
	protected static final String DEFAULT_QUERY_HANDLER_SPELL = "weblab_spell";


	/**
	 * Query handler that allows "more like this" query
	 */
	protected static final String DEFAULT_QUERY_HANDLER_MORE = "weblab_more";


	public static final String BEAN_NAME = "configBean";


	public static final String SOLR_HOME_SYSTEM_PROPERTY = "solr.solr.home";


	public static final String FIELD_ID = "id";


	public static final String FIELD_TEXT = "text";


	public static final String SOLR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";


	public static final String FIELD_SEPARATOR = ":";


	protected final Log logger = LogFactory.getLog(this.getClass());


	protected String queryHandlerMinimal = SolrConfig.DEFAULT_QUERY_HANDLER_MINIMAL;


	protected String queryHandlerMeta = SolrConfig.DEFAULT_QUERY_HANDLER_META;


	protected String queryHandlerHighLight = SolrConfig.DEFAULT_QUERY_HANDLER_HIGHLIGHT;


	protected String queryHandlerFacet = SolrConfig.DEFAULT_QUERY_HANDLER_FACET;


	protected String queryHandlerSpell = SolrConfig.DEFAULT_QUERY_HANDLER_SPELL;


	protected String queryHandlerMore = SolrConfig.DEFAULT_QUERY_HANDLER_MORE;


	protected boolean noCore = false;


	protected String zooKeeperEnsemble;


	/**
	 * Collection where indexed document will go
	 */
	protected String collectionContext = "_default";


	/**
	 * Util map to get field name based on property URI indexed. Watch out: the
	 * map is loaded only through setFields() method (ie on loading from conf
	 * file) but
	 * is not synchronized later on if the field list is changed.
	 */
	protected Map<String, Field> propertyToFieldMap = new HashMap<>();


	/**
	 * Fields used by the index
	 */
	protected List<Field> fields;


	public List<Field> getFields() {

		return this.fields;
	}


	public void setFields(final List<Field> fields) {

		this.fields = fields;
		if (fields != null) {
			for (final Field f : fields) {
				// mapping entity type in priority
				if (f.getEntityTypes() != null) {
					for (final String entityType : f.getEntityTypes()) {
						this.propertyToFieldMap.put(entityType, f);
					}
				} else {
					// if there isn't any entity type then map on property
					if (f.getProperties() != null) {
						for (final String property : f.getProperties()) {
							this.propertyToFieldMap.put(property, f);
						}
					}
				}

			}
		}
	}


	public Map<String, Field> getPropertyToFieldMap() {

		return this.propertyToFieldMap;
	}


	public void setPropertyToFieldMap(final Map<String, Field> propertyToFieldMap) {

		this.propertyToFieldMap = propertyToFieldMap;
	}


	public boolean isNoCore() {

		return this.noCore;
	}


	public void setNoCore(final boolean noCore) {

		this.noCore = noCore;
	}


	public String getQueryHandlerMinimal() {

		return this.queryHandlerMinimal;
	}


	public void setQueryHandlerMinimal(final String queryHandlerMinimal) {

		this.queryHandlerMinimal = queryHandlerMinimal;
	}


	public String getQueryHandlerMeta() {

		return this.queryHandlerMeta;
	}


	public void setQueryHandlerMeta(final String queryHandlerMeta) {

		this.queryHandlerMeta = queryHandlerMeta;
	}


	public String getQueryHandlerHighLight() {

		return this.queryHandlerHighLight;
	}


	public void setQueryHandlerHighLight(final String queryHandlerHighLight) {

		this.queryHandlerHighLight = queryHandlerHighLight;
	}


	public String getQueryHandlerFacet() {

		return this.queryHandlerFacet;
	}


	public void setQueryHandlerFacet(final String queryHandlerFacet) {

		this.queryHandlerFacet = queryHandlerFacet;
	}


	public String getQueryHandlerSpell() {

		return this.queryHandlerSpell;
	}


	public void setQueryHandlerSpell(final String queryHandlerSpell) {

		this.queryHandlerSpell = queryHandlerSpell;
	}


	public String getQueryHandlerMore() {

		return this.queryHandlerMore;
	}


	public void setQueryHandlerMore(final String queryHandlerMore) {

		this.queryHandlerMore = queryHandlerMore;
	}


	public String getZooKeeperEnsemble() {

		return this.zooKeeperEnsemble;
	}


	public void setZooKeeperEnsemble(final String zooKeeperEnsemble) {

		this.zooKeeperEnsemble = zooKeeperEnsemble;
	}



	public String getCollectionContext() {

		return this.collectionContext;
	}



	public void setCollectionContext(final String collectionContext) {

		this.collectionContext = collectionContext;
	}


}
