/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.indexer;

import org.ow2.weblab.services.solr.SolrConfig;

/**
 * @deprecated use directly org.ow2.weblab.services.solr.SolrConfig instead.
 * @author gdupont - WebLab team
 *       WebLab model 1.2
 */
@Deprecated
public class SolrIndexerConfig extends SolrConfig {
	// This has been deprecated. Please use directly SolrConfig instead.
}
