/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.indexer;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.handler.extraction.ExtractionDateUtil;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;
import org.ow2.weblab.util.index.Field;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * Indexer using Embedded Solr server. This class only check index arguments and
 * call Solr component
 */

@WebService(endpointInterface = "org.ow2.weblab.core.services.Indexer")
public class SolrIndexer implements Indexer {

    public static final char DEFAULT_DECIMAL_SEP = ',';

    public static final String BEAN_NAME = "indexerServiceBean";

    private final Log logger = LogFactory.getLog(SolrIndexer.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat(SolrConfig.SOLR_DATE_FORMAT);
    private final SimpleDateFormat dateFormatISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
    private final SimpleDateFormat dateFormatISO8601_variant_ms = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");

    private final DecimalFormat decimalFormat = new DecimalFormat();
    private final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();

    private Set<String> formats;

    private SolrConfig conf;

    @PostConstruct
    public void init() {
        this.dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        this.formats = new HashSet<>();
        this.formats.add(this.dateFormat.toPattern());
        this.formats.add(this.dateFormatISO8601.toPattern());
        this.formats.add(this.dateFormatISO8601_variant_ms.toPattern());
        this.decimalFormatSymbols.setDecimalSeparator(DEFAULT_DECIMAL_SEP);
        this.decimalFormat.setDecimalFormatSymbols(this.decimalFormatSymbols);
    }

    /**
     * @return The conf
     * @deprecated use SolrIndexer.getConf() instead
     */
    @Deprecated
	public SolrConfig getIndexerConfig() {
        return this.conf;
    }

    /**
     * @param indexerConfig The conf
     * @deprecated use SolrIndexer.setConf() instead
     */
    @Deprecated
	public void setIndexerConfig(SolrConfig indexerConfig) {
        this.conf = indexerConfig;
    }

    @PreDestroy
    public void destroy() {
        // destroy what ?
    }

    @Override
    public IndexReturn index(final IndexArgs args) throws UnexpectedException, InvalidParameterException, ServiceNotConfiguredException {

        final MediaUnit unit = this.checkArgs(args);
        
        this.logger.debug("'<"+Constants.SERVICE_NAME+" : Indexer>'"+" Start processing document '<"+unit.getUri()+">'" + " - " + "'<"+ new DublinCoreAnnotator(unit).readSource().firstTypedValue()+">'");
        
        if ((unit instanceof Audio) || (unit instanceof Video) || (unit instanceof Image)) {
            this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document "+"'<"+unit.getUri()+">'. " +"MediaUnit of type: " + unit.getClass().getSimpleName() + " cannot be indexed with Solr.");
            return new IndexReturn();
        }
        
        try {
            // convert WebLab resource into SolrDocument
            final SolrInputDocument doc = this.convertMediaUnit(unit);
            SolrComponent instance;
            if (this.conf.isNoCore()) {
                instance = SolrComponent.getInstance(this.conf, null);
            } else {
            	if (args.getUsageContext() != null)
            		instance = SolrComponent.getInstance(this.conf, args.getUsageContext());
            	else
            		instance = SolrComponent.getInstance(this.conf, this.getConf().getCollectionContext());
            }
            instance.addDocument(doc);

        } catch (final WebLabCheckedException e) {
            final String message = "Cannot index document " + unit.getUri();
            throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"'<"+unit.getUri()+">'. " +message, e);
        }
        this.logger.debug("'<"+Constants.SERVICE_NAME+" : Indexer>'"+" Start processing document '<"+unit.getUri()+">'" + " - " + "'<"+ new DublinCoreAnnotator(unit).readSource().firstTypedValue()+">'");
        return new IndexReturn();
    }

    /**
     * @param args
     *            The <code>IndexArgs</code> to check in the begin of <code>index</code>.
     * @return The contained <code>MediaUnit</code>
     * @throws InvalidParameterException
     *             if we are unable to extract the contained <code>MediaUnit</code>
     */
    protected MediaUnit checkArgs(final IndexArgs args) throws InvalidParameterException {
        if (args == null) {
            final String message = "IndexArgs was null.";
            throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
        }
        if (args.getResource() == null) {
            final String message = "Args must contain a non-null Resource to index";
            throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
        }
        if (!(args.getResource() instanceof MediaUnit)) {
            final String message = "Resource to index is not a MediaUnit.";
            throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
        }
        return (MediaUnit) args.getResource();
    }

    protected SolrInputDocument convertMediaUnit(final MediaUnit unit) throws WebLabCheckedException {
        // simple case : one SolrDocument for each WebLab Document
        final SolrInputDocument doc = new SolrInputDocument();
        try {
            new URI(unit.getUri());
        } catch (final URISyntaxException e) {
            throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Resource " + unit.getUri() + "to index does not have a valid URI.", e);
        }
        doc.addField(SolrConfig.FIELD_ID, unit.getUri());

        final JenaResourceHelper hlpr = new JenaResourceHelper(unit);

        for (final Field field : this.conf.getFields()) {
            // index text content if needed
            if (field.isIndexTextContent()) {
                this.addTextToField(doc, field.getName(), unit);
            }

            // index RDF properties if some are associated to the field
            if ((field.getProperties() != null) && (field.getProperties().size() > 0)) {
                // select RDF properties to index
                final Set<String> subjects = new HashSet<>();
                for (final String prop : field.getProperties()) {
                    subjects.addAll(hlpr.getSubjsOnPred(prop));
                }

                if(field.isRestrictSubjectToDocument()){
                    if(subjects.contains(unit.getUri())){
                        subjects.clear();
                        subjects.add(unit.getUri());
                    }else{
                        subjects.clear();
                        this.logger.trace("The field ["+field.getName()+" is restricted to Document, but no value found.");
                    }
                }

                // check if we need to restrict to a sub list of typed RDF resources
                if ((field.getEntityTypes() != null) && (field.getEntityTypes().size() > 0)) {
                    final Set<String> validEntityURIs = new HashSet<>();
                    for (final String entityTypeURI : field.getEntityTypes()) {
                        validEntityURIs.addAll(hlpr.getSubjsOnPredRes(RDF.TYPE, entityTypeURI));
                    }
                    subjects.retainAll(validEntityURIs);
                }

                // index the property values from the selected subjects
                for (final String subject : subjects) {
                    /*
                     * if asked by the field configuration, check if this is a
                     * candidate
                     */
                    boolean isCandidate = true;
                    if (field.isIndexNonCandidateInstanceAsUri()) {
                        final List<String> candidateList = hlpr.getLitsOnPredSubj(subject, WebLabProcessing.IS_CANDIDATE);
                        if ((candidateList != null) && (candidateList.size() > 0)) {
                            isCandidate = Boolean.parseBoolean(candidateList.get(0));
                        }
                    }
                    if (field.isIndexTypeOnly()) {
                        /*
                         * only indexes types of this subject which match types
                         * configured
                         */
                        final List<String> types = hlpr.getRessOnPredSubj(subject, RDF.TYPE);
                        for (final String type : field.getEntityTypes()) {
                            if (types.contains(type)) {
                                this.addFieldValue(doc, field, type);
                            }
                        }

                    } else if (isCandidate) {
                        for (final String property : field.getProperties()) {
                            final List<String> values = hlpr.getLitsOnPredSubj(subject, property);
                            // adding the values to the doc field : note that
                            // should work fine with multi-valued fields for
                            // non-multi-valued field this may raise issue since
                            // only the last value may be taken into account
                            // (but then it should have been multi-valued).
                            for (final String value : values) {
                                this.addFieldValue(doc, field, value.trim());
                            }
                        }
                    } else {
                        this.addFieldValue(doc, field, subject);
                    }
                }
            }
        }
        return doc;
    }

    protected void addFieldValue(final SolrInputDocument doc, final Field field, final String value) throws WebLabCheckedException {
        switch (field.getType()) {
        case DATE:
            Date d = null;
            synchronized (this.dateFormatISO8601) {
                try {
                    d = this.dateFormatISO8601.parse(value);
                } catch (final ParseException e) {
                    this.logger.trace("The following date [" + value + "] passed as [" + field.getName()
                            + "] is using extended ISO8601 format with milliseconds. We will make it work... maybe not to that level of millisecond precision.");
                    try {
                        d = ExtractionDateUtil.parseDate(value, this.formats);
                    } catch (final ParseException exp) {
                        throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot convert field ("+field.getName()+") value [" + value + "] to date using " + this.formats, exp);
                    }
                }
            }
            String theDate = null;
            synchronized (this.dateFormat) {
                theDate = this.dateFormat.format(d);
            }
            doc.addField(field.getName(), theDate);

            break;
        case BOOLEAN:
        	// returns true only if value is the string true (ignoring case), returns false for any other value (null or not)
        	boolean bool = Boolean.parseBoolean(value);
            doc.addField(field.getName(), Boolean.valueOf(bool));
            break;
        case LONG:
            long l;
            try {
                l = Long.parseLong(value);
            } catch (final NumberFormatException e) {
                throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot convert field ("+field.getName()+") value [" + value + "] to long.", e);
            }
            doc.addField(field.getName(), Long.valueOf(l));
            break;
        case DOUBLE:
            double doubl;
            try {
                // TODO make it locale safe by testing a lot of different double format and not just english and french
				doubl = formatDouble(value);
            } catch (final NumberFormatException | ParseException e) {
                throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot convert field ("+field.getName()+") value [" + value + "] to double.", e);
            }
            doc.addField(field.getName(), Double.valueOf(doubl));
            break;
        case INTEGER:
            int i;
            try {
                i = Integer.valueOf(value).intValue();
            } catch (final NumberFormatException e) {
                throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot convert field ("+field.getName()+") value [" + value + "] to int.", e);
            }
            doc.addField(field.getName(), Integer.valueOf(i));
            break;
        default:
            // case RES_URI:
            // case URI:
            // case TEXT:
            doc.addField(field.getName(), value);
        }

    }

	protected double formatDouble(final String value) throws ParseException {
		double doubl;
		if(value.contains(".")){
		    this.logger.trace("Field value [" + value + "] is supposed to be a double. We will try the JAVA standard format.");
		    doubl = Double.parseDouble(value);
		}else{
			synchronized(this.decimalFormat) {
			    doubl = this.decimalFormat.parse(value).doubleValue();
			}
		}
		return doubl;
	}

    /**
     *
     * @param doc
     *            is the SolRDocument to be indexed
     * @param name
     *            is the field name
     * @param unit
     *            is where the text content should be found
     */
    protected void addTextToField(final SolrInputDocument doc, final String name, final MediaUnit unit) {
        if (unit instanceof Document) {
            final Document document = (Document) unit;
            for (final MediaUnit u : document.getMediaUnit()) {
                this.addTextToField(doc, name, u);
            }
        } else if (unit instanceof Text) {
            final Text text = (Text) unit;
            doc.addField(name, text.getContent());
        } else {
            this.logger.trace("Cannot extract text content from [" + unit.getClass().getSimpleName() + "].");
        }
    }

    public SolrConfig getConf() {
        return this.conf;
    }

    public void setConf(final SolrConfig conf) {
        this.conf = conf;
    }
}