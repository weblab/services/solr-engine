/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class SpellSuggestion implements Analyser {


	public static final String BEAN_NAME = "spellSuggestionServiceBean";


	private static final String IDRES_SPELL_PREFIX = "spell";


	private static long suggestionCounter = 0;


	private static long queryCounter = 0;


	private Log logger;


	private SolrConfig conf;


	private int nbSuggestion = 10;


	@PostConstruct
	public void init() {
		this.logger = LogFactory.getLog(SpellSuggestion.class);
	}


	@PreDestroy
	public void destroy() {
		this.logger.info("Destroying SolR SpellSuggestion service.");

		// nothing to do I guess

	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException,
			ContentNotAvailableException, InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {
		this.checkArgs(args);

		final ProcessReturn ret = new ProcessReturn();

		try {
			if (args.getResource() instanceof ResultSet) {
				final ResultSet set = (ResultSet) args.getResource();
				final ComposedResource collec = this.doSpellSuggest(args.getUsageContext(), set);
				set.getResource().add(collec);
				ret.setResource(set);
			} else if (args.getResource() instanceof StringQuery) {
				final StringQuery q = (StringQuery) args.getResource();
				final ComposedResource collec = this.doSpellSuggest(args.getUsageContext(), q);
				ret.setResource(collec);
			}

		} catch (final WebLabCheckedException e) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Cannot make spell suggestion : " + e.getMessage(), e);
		}

		return ret;
	}


	public ComposedResource doSpellSuggest(final String usageContext, final ResultSet set) throws WebLabCheckedException {
		if (!(set.getResource().get(0) instanceof StringQuery)) {
			throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There is no query in the ResultSet.");
		}
		final StringQuery q = (StringQuery) set.getResource().get(0);
		return this.doSpellSuggest(usageContext, q);
	}


	public ComposedResource doSpellSuggest(final String usageContext, final StringQuery q) throws WebLabCheckedException {
		final ComposedResource collec = WebLabResourceFactory.createResource(SolrComponent.IDREF, SpellSuggestion.IDRES_SPELL_PREFIX
				+ SpellSuggestion.suggestionCounter++, ComposedResource.class);
		final String originalQuery = q.getRequest();
		SolrComponent instance;
		if (this.conf.isNoCore()) {
			instance = SolrComponent.getInstance(this.conf, null);
		} else {
			instance = SolrComponent.getInstance(this.conf, usageContext);
		}
		final QueryResponse response = instance.spellSuggest(originalQuery);
		final SpellCheckResponse scr = response.getSpellCheckResponse();

		if ((scr == null) || scr.isCorrectlySpelled()) {
			this.logger.trace("The query in the ResultSetis correctly spelled.");
		} else {
			this.logger.trace("Getting spell check suggestions");
			final List<StringQuery> suggestedQueries = new ArrayList<>();

			List<Suggestion> suggestions = scr.getSuggestions();
			// testing if there is term suggested
			if (suggestions.size() == 0) {
				this.logger.trace("The query isn't correctly spelled, but we don't have any suggestion.");
			} else if (suggestions.size() == 1) {
				// if so then produce the suggested requests simply
				for (int i = 0; i < suggestions.size(); i++) {
					Suggestion suggestion=suggestions.get(i);
					this.logger.trace("Original frequency "+suggestion.getOriginalFrequency());
					List<String>alternatives=suggestion.getAlternatives();
					if(alternatives!=null) {
						for(String alt : alternatives) {
							final StringQuery str = WebLabResourceFactory.createResource(SolrComponent.IDREF, SpellSuggestion.IDRES_SPELL_PREFIX
									+ SolrComponent.IDRES_QUERY_PREFIX + SpellSuggestion.queryCounter++, StringQuery.class);
							//str.setRequest(originalQuery.replace(originalQuery.substring(suggestion.getStartOffset(), suggestion.getEndOffset()), ls.get(i)));
							str.setRequest(alt);
							suggestedQueries.add(str);
							this.logger.trace("Adding spell check suggestion [" + str.getRequest() + "]@" + str.getWeight());
						}
					}
				}
			} else {
				// if not we need to produce all possible queries and then
				// weight them to get the top N suggestions
//				final StringQuery[] topNSuggestedQueries = new StringQuery[this.nbSuggestion];
//				final double[] topN = new double[this.nbSuggestion];
//				for (final String s : mscr.keySet()) {
//					final Suggestion suggestion = mscr.get(s);
//					final List<String> ls = suggestion.getAlternatives();
//					final List<Integer> fs = suggestion.getAlternativeFrequencies();
//
//					final Set<StringQuery> suggestedQueriesToBeAdded = new HashSet<StringQuery>();
//					for (int i = 0; i < ls.size(); i++) {
//						if (suggestedQueries.size() > 0) {
//
//							for (final StringQuery query : suggestedQueries) {
//								final StringQuery str = WebLabResourceFactory.createResource(SolrComponent.IDREF, SpellSuggestion.IDRES_SPELL_PREFIX
//										+ SolrComponent.IDRES_QUERY_PREFIX + SpellSuggestion.queryCounter++, StringQuery.class);
//								str.setWeight(1f);
//								// compile the request
//								str.setRequest(query.getRequest().replace(suggestion.getToken(), ls.get(i)));
//								// average the weight
//								str.setWeight((fs.get(i).floatValue() + query.getWeight()) / 2);
//								suggestedQueriesToBeAdded.add(str);
//								this.addTopScoredSuggestion(topN, topNSuggestedQueries, str);
//							}
//
//						}
//
//						final StringQuery str = WebLabResourceFactory.createResource(SolrComponent.IDREF, SpellSuggestion.IDRES_SPELL_PREFIX
//								+ SolrComponent.IDRES_QUERY_PREFIX + SpellSuggestion.queryCounter++, StringQuery.class);
//						str.setRequest(originalQuery.replace(suggestion.getToken(), ls.get(i)));
//						str.setWeight(fs.get(i).floatValue());
//						suggestedQueries.add(str);
//						this.logger.debug("Adding spell check suggestion [" + str.getRequest() + "]@" + str.getWeight());
//
//						this.addTopScoredSuggestion(topN, topNSuggestedQueries, str);
//					}
//					if (suggestedQueriesToBeAdded.size() > 0) {
//						suggestedQueries.addAll(suggestedQueriesToBeAdded);
//					}
//				}
			}
			if (suggestedQueries.size() == 0) {
				this.logger.trace("There is no spell check suggestions done despite the suggested terms...");
			} else {
				this.logger.trace("Adding spell check suggestions to result set.");
				collec.getResource().addAll(suggestedQueries);
			}
		}
		this.logger.trace("Spell suggestion done for [" + q.getUri() + "].");

		return collec;
	}


//	private void addTopScoredSuggestion(final double[] topN, final StringQuery[] topNSuggestedQueries, final StringQuery str) {
//		for (int i = 0; i < topN.length; i++) {
//			if (topN[i] < str.getWeight()) {
//				topN[i] = str.getWeight();
//				topNSuggestedQueries[i] = str;
//			}
//		}
//	}


	private void checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			String message = "Input args for SpellSuggestion cannot be null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		// TODO finish it...
	}


	public int getNbSuggestion() {
		return this.nbSuggestion;
	}


	public void setNbSuggestion(final int nbSuggestion) {
		this.nbSuggestion = nbSuggestion;
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}

}
