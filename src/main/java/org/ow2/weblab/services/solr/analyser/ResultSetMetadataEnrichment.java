/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;
import org.ow2.weblab.util.index.Field;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class ResultSetMetadataEnrichment implements Analyser {


	public static final String BEAN_NAME = "resultSetMetaEnricherServiceBean";


	private Log logger;


	private SolrConfig conf;


	private List<Field> fields;


	private SimpleDateFormat dateFormat;


	@PostConstruct
	public void init() {
		this.logger = LogFactory.getLog(ResultSetMetadataEnrichment.class);
		// init date formatter
		this.dateFormat = new SimpleDateFormat(SolrConfig.SOLR_DATE_FORMAT);
//		try {
//			new URL(this.conf.getSolrURL());
//		} catch (final MalformedURLException e) {
//			throw new WebLabUncheckedException("Cannot start the service. The solrULR is invalid [" + this.conf.getSolrURL() + "].", e);
//		}
	}


	@PreDestroy
	public void destroy() {
		// nothing to do I guess
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException,
			ContentNotAvailableException, InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {

		this.checkArgs(args);

		final ResultSet enrichSet = this.addMetadataToResultSet(args.getUsageContext(), (ResultSet) args.getResource());

		final ProcessReturn ret = new ProcessReturn();

		ret.setResource(enrichSet);
		return ret;
	}


	private void checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "Input args for [" + this.getClass().getSimpleName() + "] cannot be null.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		// TODO finish it...
	}


	public ResultSet addMetadataToResultSet(final String usageContext, final ResultSet resultSet) throws UnexpectedException {
		try {
			final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(resultSet.getUri()), resultSet.getPok());

			final JenaPoKHelper hlpr = new JenaPoKHelper(resultSet.getPok(), false);

			final List<URI> docUris = new ArrayList<>();
			if ((wra.readHit() == null) || (wra.readHit().size() == 0)) {
				this.logger.trace("No hit in the resultSet.");
			} else {
				final Value<URI> hits = wra.readHit();
				for (final URI hit : hits) {
					wra.startInnerAnnotatorOn(hit);
					docUris.addAll(wra.readLinkedTo().getValues());
					wra.endInnerAnnotator();
				}
				final String[] uris = new String[docUris.size()];
				for (int k = 0; k < docUris.size(); k++) {
					uris[k] = docUris.get(k).toString();
				}
				SolrComponent instance;
				if (this.conf.isNoCore()) {
					instance = SolrComponent.getInstance(this.conf, null);
				} else {
					instance = SolrComponent.getInstance(this.conf, usageContext);
				}
				final QueryResponse response = instance.getMetaData(uris);

				if ((response.getResults() == null) || (response.getResults().size() == 0)) {
					throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot get metadata from Solr.");
				}
				if (response.getResults().size() < docUris.size()) {
					this.logger.trace("Solr returned only " + response.getResults().size() + "... We'll do what we can.");
				}

				for (final SolrDocument doc : response.getResults()) {
					for (final Field field : this.fields) {
						this.addFieldDataToPok(hlpr, (String) doc.getFieldValue(SolrConfig.FIELD_ID), doc, field);
					}
				}
				hlpr.commit();
			}
			this.logger.trace("Metadata enrichment done for [" + resultSet.getUri() + "].");
		} catch (final WebLabCheckedException e) {
			final String message = "Well metadata enrichment failed: " + e.getMessage();
			throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message, e);
		}
		return resultSet;
	}


	private void addFieldDataToPok(final JenaPoKHelper hlpr, final String docURI, final SolrDocument doc, final Field field) throws WebLabCheckedException {
		if (field.getProperties().size() == 1) {
			if (doc.getFieldValue(field.getName()) == null) {
				this.logger.trace("Cannot get metadata from field [" + field.getName() + "]. It does not contain anything.");
			} else {
				for (final Object value : doc.getFieldValues(field.getName())) {
					switch (field.getType()) {
						case DATE:
							final Date d = (Date) value;
							hlpr.createLitStat(docURI, field.getProperties().get(0).trim(), this.dateFormat.format(d));
							break;
						case RES_URI:
							hlpr.createResStat(docURI, field.getProperties().get(0).trim(), value.toString());
							break;
						// case URI:
						// case LONG:
						// case TEXT:
						default:
							hlpr.createLitStat(docURI, field.getProperties().get(0).trim(), value.toString());
					}
				}
			}
		} else {
			// that should be a text only field or a field that merge multiple
			// properties => it should not be in the list for metadata
			// enrichment.
			throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot get metadata from field [" + field.getName()
					+ "]. It should not be in the metadata enrichmenet field list.");
		}
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}


	public List<Field> getFields() {
		return this.fields;
	}


	public void setFields(final List<Field> fields) {
		this.fields = fields;
	}

}
