/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.SolrQueryParser;
import org.ow2.weblab.util.WebLabQueryParser;
import org.ow2.weblab.util.constants.Constants;
import org.ow2.weblab.util.index.Field;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class FacetSuggestion implements Analyser {


	protected static final String FILTER_TAG = "/filter";


	public static final String BEAN_NAME = "facetSuggestionServiceBean";


	public static final String IDRES_FACET_PREFIX = "facets";


	protected static long suggestionCounter = 0;


	protected static long queryCounter = 0;


	protected Log logger;


	protected SolrConfig conf;


	private Set<Field> facetFields;


	private final Map<String, String> fieldToPropertyMap = new HashMap<>();


	private WebLabQueryParser parser;


	@PostConstruct
	public void init() {
		this.logger = LogFactory.getLog(FacetSuggestion.class);
		this.parser = new SolrQueryParser(this.conf);

		for (final Field f : this.facetFields) {
			if ((f.getProperties() != null) && (f.getProperties().size() > 0)) {
				// TODO find another way since it does not take into account the mapping from field to property which is 1..*

				if ((f.getEntityTypes() != null) && (f.getEntityTypes().size() > 0)) {
					// use entity type filter as scope
					this.fieldToPropertyMap.put(f.getName(), f.getEntityTypes().get(0));
				} else {
					// use property filter as scope
					this.fieldToPropertyMap.put(f.getName(), f.getProperties().get(0));
				}
			}
		}
	}


	@PreDestroy
	public void destroy() {
		this.logger.trace("Destroying SolR FacetSuggestion service.");
		// nothing to do I guess
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException,
			ContentNotAvailableException, InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {

		this.checkArgs(args);

		final ResultSet enrichSet = this.doFacetSuggest(args.getUsageContext(), (ResultSet) args.getResource());

		final ProcessReturn ret = new ProcessReturn();
		ret.setResource(enrichSet);
		return ret;
	}


	public ResultSet doFacetSuggest(final String usageContext, final ResultSet set) {

		try {
			if (!(set.getResource().get(0) instanceof Query)) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There is no query in the ResultSet.");
			}
			final Query query = (Query) set.getResource().get(0);
			final ComposedResource cres = this.doFacetSuggest(usageContext, query);
			set.getResource().add(cres);
		} catch (final WebLabCheckedException e) {
			this.logger.trace("Cannot make spell suggestion : " + e.getMessage());
		}
		return set;
	}


	public ComposedResource doFacetSuggest(final String usageContext, final Query query) {
		final ComposedResource cres = WebLabResourceFactory.createResource(SolrComponent.IDREF, FacetSuggestion.IDRES_FACET_PREFIX
				+ FacetSuggestion.suggestionCounter++, ComposedResource.class);
		try {
			SolrComponent instance;
			if (this.conf.isNoCore()) {
				instance = SolrComponent.getInstance(this.conf, null);
			} else {
				instance = SolrComponent.getInstance(this.conf, usageContext);
			}

			// check if the query has annotation to expect a specific behavior
			boolean ascOrder = false;
			String orderedByProperty = null;
			if (query.getAnnotation().size() > 0) {
				final ResourceHelper hlpr = new JenaResourceHelper(query);
				ascOrder = this.parser.getOrder(query, hlpr);
				orderedByProperty = this.parser.getOrderBy(query, hlpr);
			}
			final String request = this.parser.getRequest(query, new JenaResourceHelper(query));
			final QueryResponse response = instance.facetSuggest(request, 0, 1, orderedByProperty, ascOrder ? ORDER.asc : ORDER.desc);

			if ((response.getFacetFields() == null) || (response.getFacetFields().size() == 0)) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There are no facets in the ResultSet.");
			}

			for (final FacetField facet : response.getFacetFields()) {
				final List<Count> countList = facet.getValues();
				if (countList == null) {
					this.logger.trace("Facet " + facet.getName() + " is empty.");
				} else {
					for (final Count valuedFacet : countList) {
						final int nbRes = (int) valuedFacet.getCount();
						if (nbRes == 0) {
							this.logger.trace("Facet " + facet.getName() + ":" + valuedFacet.getName() + " is empty.");
						} else {
							try{
								final Query facetQuery = this.createFacetQuery(query, 0, facet, nbRes, valuedFacet.getName());
								cres.getResource().add(facetQuery);
							}catch(WebLabCheckedException e){
								this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Ignoring a facet received: "+e.getMessage());
							}
						}
					}
				}
			}
			this.logger.trace("Facet suggestion done for [" + query.getUri() + "].");
		} catch (final WebLabCheckedException e) {
			this.logger.trace("Cannot make facet suggestion : " + e.getMessage(), e);
		}
		return cres;
	}


	/**
	 * Create facet query in accordance to the new usage of StringQuery => issue
	 * WEBLAB-316
	 *
	 * @param query
	 *            is the original query that was sent to generate the facets
	 * @param offset
	 *            is the original offset of the query
	 * @param facet
	 *            is the fact as a FacetField
	 * @param nbRes
	 *            is the number of results associated to that facet if applied
	 *            as filter
	 * @param facetFilter
	 *            is the filter to apply as a String
	 * @return a StringQuery with all information related to scope and link in
	 *         an annotation.
	 * @throws WebLabCheckedException
	 */
	private Query createFacetQuery(final Query query, final int offset, final FacetField facet, final int nbRes, String facetFilter) throws WebLabCheckedException {
		final String cQueryIdRes = FacetSuggestion.IDRES_FACET_PREFIX + SolrComponent.IDRES_QUERY_PREFIX + FacetSuggestion.queryCounter++;

		// create facet filter
		final StringQuery facetQuery = WebLabResourceFactory.createResource(SolrComponent.IDREF, cQueryIdRes + FacetSuggestion.FILTER_TAG, StringQuery.class);
		facetQuery.setWeight(Float.valueOf(1));
		// add scope on query to filter on specific field based on facet name
		// (trying to get proper property uri instead of field name)

		if (! this.fieldToPropertyMap.containsKey(facet.getName())) {
			throw new WebLabCheckedException("The facet ["+facet.getName()+"] is not known. Cannot process it.");
		}
		String scope = this.fieldToPropertyMap.get(facet.getName());
		this.logger.trace("Creating facet on ["+scope+"].");
		new WRetrievalAnnotator(URI.create(facetQuery.getUri()), WebLabResourceFactory.createAndLinkAnnotation(facetQuery)).writeScope(URI.create(scope));
		// ensure expression are expressed with quotes
		if (!facetFilter.contains("\"")) {
			facetFilter = "\"" + facetFilter + "\"";
		}else{
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Facet filter proposed contain quotes ["+facetFilter+"]. This may lead to invalid query.");
		}
		// set filter
		facetQuery.setRequest(facetFilter);

		// combine original query and filter in one ComposedQuery
		final ComposedQuery cQuery = WebLabResourceFactory.createResource(SolrComponent.IDREF, cQueryIdRes, ComposedQuery.class);
		cQuery.setOperator(Operator.AND);
		cQuery.getQuery().add(query);
		cQuery.getQuery().add(facetQuery);

		// add facet information on the query
		WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(cQuery.getUri()), WebLabResourceFactory.createAndLinkAnnotation(cQuery));
		wra.writeNumberOfResults(Integer.valueOf(nbRes));
		wra.writeQueryOffset(Integer.valueOf(offset));
		wra.writeExpressedWith(this.getClass().getSimpleName());
		wra.writeLabel(facetFilter);
		wra.writeDescription(facetFilter);
		wra.writeLinkedTo(URI.create(query.getUri()));
		return cQuery;
	}


	private void checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "Input args for [" + this.getClass().getSimpleName() + "] cannot be null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		// TODO finish it...
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}


	public Set<Field> getFacetFields() {
		return this.facetFields;
	}


	public void setFacetFields(final Set<Field> facetFields) {
		this.facetFields = facetFields;
	}

}
