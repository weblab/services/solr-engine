/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr.searcher;

import java.net.URI;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.Searcher;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.services.solr.analyser.FacetSuggestion;
import org.ow2.weblab.services.solr.analyser.Highlighter;
import org.ow2.weblab.services.solr.analyser.ResultSetMetadataEnrichment;
import org.ow2.weblab.util.SolrQueryParser;
import org.ow2.weblab.util.WebLabQueryParser;
import org.ow2.weblab.util.constants.Constants;

/**
 * Searcher using Embedded Solr server.
 *
 * RDF output is generated with Jena and can be enriched with metadata stored in index. This should be define in searcher configuration file.
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Searcher")
public class SolrSearcher implements Searcher {


	public static final String BEAN_NAME = "searcherServiceBean";


	private static int resultsCounter = 0;


	private static int hitCounter = 0;


	private final Log logger = LogFactory.getLog(SolrSearcher.class);


	private SolrConfig conf;


	private WebLabQueryParser parser;


	// these two fields are only here to please Yann ;-)
	private ResultSetMetadataEnrichment enricher;


	private Highlighter highlighter;


	private FacetSuggestion facetSuggestion;


	@PostConstruct
	public void init() {
		this.parser = new SolrQueryParser(this.conf);
//		try {
//			// init default instance
//			SolrComponent.getInstance(this.conf, null);
//
//			// get indexingConfig if not loaded
//			if (this.conf == null) {
//				// get it directly ?
//			}
//
//
//
//			new URL(this.conf.getSolrURL());
//		} catch (final WebLabCheckedException e) {
//			throw new WebLabUncheckedException("Cannot start the SolrComponent.", e);
//		} catch (final MalformedURLException e) {
//			throw new WebLabUncheckedException("Cannot start the service. The solrULR is invalid [" + this.conf.getSolrURL() + "].", e);
//		}
	}


	@PreDestroy
	public void destroy() {
		// nothing to do I guess
	}


	@Override
	public SearchReturn search(final SearchArgs arg) throws InvalidParameterException, ServiceNotConfiguredException, UnexpectedException {
		this.checkArgs(arg);

		ResultSet set = this.search(arg.getUsageContext(), arg.getQuery(), arg.getOffset().intValue(), arg.getLimit().intValue());

		// //////////////////////////////////////////////////////////////////
		// retro-compatibility code for old all-in-solr model of retrieval
		// //////////////////////////////////////////////////////////////////
		if (this.enricher != null) {
			set = this.enricher.addMetadataToResultSet(arg.getUsageContext(), set);
		}
		if (this.highlighter != null) {
			set = this.highlighter.highLightHitInResultSet(arg.getUsageContext(), set);
		}
		if (this.facetSuggestion != null) {
			set = this.facetSuggestion.doFacetSuggest(arg.getUsageContext(), set);
		}
		// //////////////////////////////////////////////////////////////////
		// end of retro-compatibility code
		// //////////////////////////////////////////////////////////////////

		final SearchReturn re = new SearchReturn();
		re.setResultSet(set);
		return re;
	}


	protected void checkArgs(final SearchArgs arg) throws InvalidParameterException {
		if (arg == null) {
			final String msg = "SearchArgs was null.";
			throw new InvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. ", msg);
		}

		if(!arg.isSetLimit() || !arg.isSetOffset()){
			final String msg = "Offset and/or Limit was null.";
			throw new InvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. ", msg);
		}

		final Query q = arg.getQuery();
		if (!(q instanceof StringQuery) && !(q instanceof SimilarityQuery) && !(q instanceof ComposedQuery)) {
			final String msg = "This service " + this.getClass().getSimpleName() + " can only process " + StringQuery.class.getSimpleName() + ", "
					+ SimilarityQuery.class.getSimpleName() + " or " + ComposedQuery.class.getSimpleName() + ". Input has invalid type:"
					+ q.getClass().getSimpleName();
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+msg);
		}
	}


	/**
	 * Use <code>SolrComponent</code> for Solr index querying and format response in RDF with Jena.

	 * The ResultSet contains a Pok which himself contains Hits.
	 *
	 * If enrichment is activated, all fields presents in index and search configuration file are annotated.
	 *
	 * @param context
	 *            the usage context
	 * @param q
	 *            the Weblab <code>StringQuery</code>
	 * @param offset
	 *            results start index
	 * @param limit
	 *            results end index
	 * @return the <code>ResultSet</code> containing hits
	 * @throws InvalidParameterException If the query is not valid
	 * @throws UnexpectedException If the query is of an unhandled type
	 */
	public ResultSet search(final String context, final Query q, final int offset, final int limit) throws InvalidParameterException, UnexpectedException {

		if (q instanceof StringQuery) {
			this.logger.trace("String query request : " + ((StringQuery) q).getRequest());
		} else if (q instanceof SimilarityQuery) {
			this.logger.trace("Sim query request : " + ((SimilarityQuery) q).getResource().size());
		} else if (q instanceof ComposedQuery) {
			this.logger.trace("Composed query request containing " + ((ComposedQuery) q).getQuery().size() + " queries combined with "
					+ ((ComposedQuery) q).getOperator());
		}
		if (this.logger.isTraceEnabled()) {
			try {
				this.logger.trace("Query input : " + ResourceUtil.saveToXMLString(q));
			} catch (final WebLabCheckedException e) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Error when logging query input ( " + q.getUri() + ").", e);
			}
		}

		final SolrComponent instance;
		if (this.conf.isNoCore()) {
			instance = SolrComponent.getInstance(this.conf, null);
		} else {
			instance = SolrComponent.getInstance(this.conf, context);
		}

		final QueryResponse res;

		final ResourceHelper hlpr = new JenaResourceHelper(q);

		// check if the query has annotation to expect a specific behavior
		boolean ascOrder = false;
		String orderedByProperty = null;
		if (q.getAnnotation().size() > 0) {
			try {
				ascOrder = this.parser.getOrder(q, hlpr);
			} catch (final WebLabCheckedException e) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Error reading Order on query " + q.getUri() + "). Using default value (false).", e);
			}
			try {
				orderedByProperty = this.parser.getOrderBy(q, hlpr);
			} catch (final WebLabCheckedException e) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Error reading orderedBy Property on query " + q.getUri() + "). Using default value (null).", e);
			}
		}

		try {
			if (q instanceof StringQuery) {
				final String theRequest = this.parser.getRequestWithScope(q, hlpr);
				// querying solr engine with simple text query
				res = instance.search(theRequest, offset, limit, orderedByProperty, ascOrder ? ORDER.asc : ORDER.desc);
			} else if (q instanceof SimilarityQuery) {
				// launching the similarity query for solr
				res = this.launchSimilarityQuery((SimilarityQuery) q, offset, limit, instance);
			} else if (q instanceof ComposedQuery) {
				// processing and launching composed query
				res = instance.search(this.parser.getComposedRequest((ComposedQuery) q, hlpr), offset, limit, orderedByProperty, ascOrder ? ORDER.asc
						: ORDER.desc);
			} else {
				// this should never appear, but we never know...
				final String message = "Query was not of the right type but was gone through checkArgs. This is impossible..";
				this.logger.error(message);
				throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
			}
		} catch (final WebLabCheckedException e) {
			final String message = "Cannot retrieve the results to query [" + q.getUri() + "] - " + e.getMessage();
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+message, e);
		}


			final SolrDocumentList resultDocs = res.getResults();
			int resCpt = 0;

			final ResultSet results = WebLabResourceFactory.createResource(SolrComponent.IDREF, SolrComponent.IDRES_RESULT_PREFIX
					+ SolrSearcher.resultsCounter++, ResultSet.class);
			// adding the original query as first resource in the resultSet
			results.getResource().add(q);
			final URI queryURI = URI.create(q.getUri());

			// adding pok
			final PieceOfKnowledge pok = WebLabResourceFactory.createAndLinkPoK(results);
			results.setPok(pok);
			// starting to write RDF information in pok
			final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(results.getUri()), pok);

			// Set Result type property
			wra.writeType(URI.create(WebLabModel.RESULT_SET));
			// Set ResultSet "isResultOf" property
			wra.writeResultOf(queryURI);
			// writing expected offset/limit
			wra.writeExpectedOffset(Integer.valueOf(offset));
			wra.writeExpectedLimit(Integer.valueOf(limit));

			// set ascendent ordering if necessary
			if (ascOrder) {
				wra.writeRankedAscending(Boolean.TRUE);
			}

			// check if there are some results
			if ((resultDocs == null) || (resultDocs.getNumFound() == 0)) {
				// adding no results information
				wra.writeNumberOfResults(Integer.valueOf(0));
			} else {
				// adding total number of results annotation
				wra.writeNumberOfResults(Integer.valueOf((int) resultDocs.getNumFound()));
				// create Hit annotations for each result
				for (final SolrDocument hit : resultDocs) {
					final URI resourceUri = URI.create(String.valueOf(hit.getFieldValue("id")));
					final URI hitUri = URI.create("weblab://" + SolrComponent.IDREF + "/" + SolrComponent.IDRES_HIT_PREFIX + SolrSearcher.hitCounter++);
					// linking resultSet to the Hit
					wra.writeHit(hitUri);
					// starting Hit specific annotation
					wra.startInnerAnnotatorOn(hitUri);
					// adding type annotation
					wra.writeType(URI.create(WebLabRetrieval.HIT));
					// adding hasRank annotation
					wra.writeRank(Integer.valueOf(offset + resCpt + 1));
					// adding solr score annotation
					wra.writeScore(Double.valueOf(((Float) hit.getFieldValue("score")).floatValue()));
					// adding link to resource
					wra.writeLinkedTo(resourceUri);

					wra.endInnerAnnotator();
					resCpt++;
				}
			}

			if (this.logger.isTraceEnabled()) {
				try {
					this.logger.trace("Results input : " + ResourceUtil.saveToXMLString(results));
				} catch (WebLabCheckedException e) {
					this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"An error occured while trying to marshall the resultset as string.",e);
				}
			}

			return results;
	}


	/**
	 * Create the query adapted to Solr - ie making use of more-like-this
	 * function.
	 *
	 * @param q
	 *            the SimilarityQuery
	 * @param offset
	 *            as int
	 * @param limit
	 *            as int
	 * @param instance
	 *            of SolrComponent
	 * @return a Solr QueryResponse
	 * @throws WebLabCheckedException
	 */
	private QueryResponse launchSimilarityQuery(final SimilarityQuery q, final int offset, final int limit, final SolrComponent instance)
			throws WebLabCheckedException {
		QueryResponse res;
		final List<Resource> resources = q.getResource();

		if (resources.size() == 1) {
			// single sample => can use more like this function
			res = instance.moreLikeThis(this.parser.getRequest(q, new JenaResourceHelper(q)), offset, limit);
		} else {
			// multiple samples, thus try simple ID based query
			// TODO check this is the expected behavior
			res = instance.search(this.parser.getRequest(q, new JenaResourceHelper(q)), offset, limit);
		}
		return res;
	}


	public ResultSetMetadataEnrichment getEnricher() {
		return this.enricher;
	}


	public void setEnricher(final ResultSetMetadataEnrichment enricher) {
		this.enricher = enricher;
	}


	public Highlighter getHighlighter() {
		return this.highlighter;
	}


	public void setHighlighter(final Highlighter highlighter) {
		this.highlighter = highlighter;
	}


	public FacetSuggestion getFacetSuggestion() {
		return this.facetSuggestion;
	}


	public void setFacetSuggestion(final FacetSuggestion facetSuggestion) {
		this.facetSuggestion = facetSuggestion;
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}

}
