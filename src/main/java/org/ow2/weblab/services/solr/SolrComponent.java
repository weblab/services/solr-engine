/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.solr;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrQuery.SortClause;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.CoreAdminParams.CoreAdminAction;
import org.apache.solr.common.params.MoreLikeThisParams;
import org.apache.solr.common.util.NamedList;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabNotYetImplementedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.util.constants.Constants;

/**
 * Component used to :
 * <ul>
 * <li>open a connection to remote solr server</li>
 * <li>add a document to the index</li>
 * <li>search in the index by text and structured queries</li>
 * <li>search documents similar to an existing document (more like this)</li>
 * <li>delete one document from the index</li>
 * <li>drop a complete index</li>
 * </ul>
 *
 * This class implements singleton pattern in order to share index connection and improve performance (Solr server is thread-safe).
 * 
 * 
 * The index buffer is managed by Solr so we just use a counter to commit documents when buffer configured size is achieved.
 */

public class SolrComponent {

	// //////////////////////////////////////////////////////////////////
	// static part
	// //////////////////////////////////////////////////////////////////

	/**
	 * Service ID reference
	 */
	public static final String IDREF = "indexsearch.solr";

	/**
	 * Service ID-res prefix for resultSet
	 */
	public static final String IDRES_RESULT_PREFIX = "result";

	/**
	 * Service ID-res prefix for Query
	 */
	public static final String IDRES_QUERY_PREFIX = "query";

	/**
	 * Service ID-res prefix for Hit
	 */
	public static final String IDRES_HIT_PREFIX = "hit";

	/**
	 * Default core prefix
	 */
	public static final String DEFAULT_CORE_PREFIX = "core";

	/**
	 * Default core suffix
	 */
	public static final String DEFAULT_CORE_SUFFIX = "_default";

	/**
	 * These are the parameters for the remote Solr server used. As they are protected, one could change them by overriding the values. However this is not
	 * recommended and the client should use default values.
	 */
	protected static String SOLR_HOME = "./";

	protected static String SOLR_DATA_HOME = SolrComponent.SOLR_HOME;

	protected static String SOLR_CONFIG_FILE = "solrconfig.xml";

	protected static String SOLR_SCHEMA_FILE = "schema.xml";

	protected static SolrComponent defaultInstance;

	protected static final Map<String, SolrComponent> instancesByContext = Collections.synchronizedMap(new HashMap<String, SolrComponent>());

	/**
	 * Retrieve SolrComponent instance depending on context
	 *
	 * @param conf The configuration
	 * @param context The context
	 * @return the <code>SolrComponent</code> associated to the context
	 */
	public static synchronized SolrComponent getInstance(final SolrConfig conf, final String context) {
		if ((context == null) || (context.trim().isEmpty())) {
			if (SolrComponent.defaultInstance == null) {
				SolrComponent.defaultInstance = new SolrComponent(conf, null);
			}
			return SolrComponent.defaultInstance;
		}

		if (!SolrComponent.instancesByContext.containsKey(context)) {
			SolrComponent.instancesByContext.put(context, new SolrComponent(conf, context));
		}
		return SolrComponent.instancesByContext.get(context);
	}

	/**
	 * This method is not yet implemented!
	 *
	 * @param context The usage context
	 * @exception WebLabNotYetImplementedException Not yet implemented
	 */
	public static synchronized void deleteInstance(final String context) {
		String message = "Instance deletion is not yet implemented. The current solution is to do it manually on the server.";
		LogFactory.getLog(SolrComponent.class).error(message);
		throw new WebLabNotYetImplementedException(message);
	}


	// //////////////////////////////////////////////////////////////////
	// instance part
	// //////////////////////////////////////////////////////////////////

	protected final Log logger;

	protected SolrConfig conf;

	protected String coreName;

	protected String coreDataHome;

	protected SolrClient server;

	protected int nbDocSinceLastFlush = 0;

	protected String queryHandlerMinimal;
	protected String queryHandlerMeta;
	protected String queryHandlerHighLight;
	protected String queryHandlerFacet;
	protected String queryHandlerSpell;
	protected String queryHandlerMore;

	/**
	 * Extra constructor which enables to used non default parameters.
	 *
	 * @param context which will define the SolR core to be used
	 * @param config defining the solr server configuration
	 */
	protected SolrComponent(final SolrConfig config, final String context) {
		this.logger = LogFactory.getLog(this.getClass());

		// init core variables
		this.coreName = DEFAULT_CORE_PREFIX + (context == null ? DEFAULT_CORE_SUFFIX : "" + context);
		this.conf = config;

		// open solr server to that core
		this.open();

		this.queryHandlerMinimal = this.conf.getQueryHandlerMinimal();
		this.queryHandlerMeta = this.conf.getQueryHandlerMeta();
		this.queryHandlerHighLight = this.conf.getQueryHandlerHighLight();
		this.queryHandlerFacet = this.conf.getQueryHandlerFacet();
		this.queryHandlerSpell = this.conf.getQueryHandlerSpell();
		this.queryHandlerMore = this.conf.getQueryHandlerMore();

		this.logger.info("SolR [" + this.getCoreName() + "] is ready to serve my lord !");

	}

	/**
	 * Create the client for communication with the current core
	 */
	protected void open() {
		this.logger.trace("Connecting to SOLR server...");
		HttpClient httpClient = HttpClientUtil.createClient(null);
		HttpClientUtil.setSoTimeout(httpClient, 300000);
		HttpClientUtil.setConnectionTimeout(httpClient, 5000);
		this.server = new CloudSolrClient.Builder().withHttpClient(httpClient).withZkHost(this.conf.getZooKeeperEnsemble()).build();
		this.logger.trace("SolR client is connected to the server.");
	}

	/**
	 * Status of SolR core linked to that instance
	 *
	 * @return some information in JSON-like
	 */
	public String getStatus() {
		final CoreAdminRequest statusRequest = new CoreAdminRequest();
		statusRequest.setAction(CoreAdminAction.STATUS);
		statusRequest.setCoreName(this.getCoreName());
		try {
			final NamedList<Object> status = this.server.request(statusRequest, this.coreName);
			return status.toString();
		} catch (final IOException | SolrServerException e) {
			String message = "Error while requesting SolR [" + this.getCoreName() + "] status.";
			this.logger.error(message, e);
			throw new WebLabUncheckedException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. " + message, e);
		}
	}

	public void addDocument(final SolrInputDocument doc) throws WebLabCheckedException {
		if (doc.getField("id") == null) {
			throw new WebLabCheckedException("Document to index does not have valid ID.");
		}
		final Object id = doc.getField("id").getValue();
		this.logger.trace("Indexing document [" + id + "]...");

		try {
			this.server.add(this.coreName, doc);
			this.nbDocSinceLastFlush++;
			this.logger.trace("Resource [" + id + "] added to the indexing buffer.");
		} catch (final SolrException e) {
			throw new WebLabCheckedException("Bad request. SolrDocument " + id + "does probably not respect the Solr scheme.", e);
		} catch (final IOException e) {
			throw new WebLabCheckedException("I/O access error when adding document" + id + ".", e);
		} catch (final SolrServerException e) {
			throw new WebLabCheckedException("Server error while adding document" + id + ".", e);
		}
	}

	public void addDocuments(final Collection<SolrInputDocument> docs) throws WebLabCheckedException {
		StringBuilder ids = new StringBuilder();
		for (SolrInputDocument doc : docs) {
			if (doc.getField("id") == null) {
				throw new WebLabCheckedException("Document to index does not have valid ID.");
			}
			if (ids.length() > 0) {
				ids.append(", ");
			}
			ids.append(doc.getField("id").toString());
		}
		this.logger.trace("Indexing documents [" + ids + "]...");

		try {
			this.server.add(this.coreName, docs);
			this.nbDocSinceLastFlush += docs.size();
			this.logger.trace("Resources [" + ids + "] added to the indexing buffer.");
		} catch (final SolrException e) {
			throw new WebLabCheckedException("Bad request. SolrDocuments " + ids + "does probably not respect the Solr scheme.", e);
		} catch (final IOException e) {
			throw new WebLabCheckedException("I/O access error when adding documents : " + ids + ".", e);
		} catch (final SolrServerException e) {
			throw new WebLabCheckedException("Server error while adding documents : " + ids + ".", e);
		}
	}

	/**
	 * Commit method only flush and optimize index.
	 */
	public void commit() {
		if (this.server != null) {
			try {
				this.logger.info("Commiting last update on SolR server...");
				if (this.nbDocSinceLastFlush > 0) {
					this.server.commit(this.coreName);
					this.nbDocSinceLastFlush = 0;
					this.logger.trace("Indexing buffer flushed.");
				}
				this.server.optimize(this.coreName);
			} catch (final IOException e) {
				final String message = "I/O access error while optimizing the index.";
				throw new WebLabUncheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message, e);
			} catch (final SolrServerException e) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. Cannot optimize the index properly.", e);
			} finally {
				this.logger.trace("Commit done.");
			}
		}
	}

	/**
	 * Search method query solr server with
	 * <ul>
	 * <li>string of the query</li>
	 * <li>offset</li>
	 * <li>limit</li>
	 * </ul>
	 *
	 * @param queryString is the full-text query in Lucene syntax
	 * @param offset is the rank of the first result to be returned
	 * @param limit is the size of the results list to be returned
	 * @return the Solr <code>QueryResponse</code> containing scored hits each one being linked to the Document uri
	 * @throws WebLabCheckedException If an error occurs
	 */
	public QueryResponse search(final String queryString, final int offset, final int limit) throws WebLabCheckedException {
		return this.search(queryString, offset, limit, null, ORDER.desc);
	}

	/**
	 * Search method query solr server with
	 * <ul>
	 * <li>string of the query</li>
	 * <li>offset</li>
	 * <li>limit</li>
	 * </ul>
	 *
	 * @param queryString is the full-text query in Lucene syntax
	 * @param offset is the rank of the first result to be returned
	 * @param limit is the size of the results list to be returned
	 * @param sortField is a specific field to use for sorting results
	 * @param order is the ordering model to use for sorting: either ORDER.desc or ORDER.asc
	 *
	 * @return the Solr <code>QueryResponse</code> containing scored hits each one being linked to the Document uri
	 * @throws WebLabCheckedException If an error occurs
	 */
	public QueryResponse search(final String queryString, final int offset, final int limit, final String sortField, final ORDER order)
			throws WebLabCheckedException {
		QueryResponse response = new QueryResponse();
		if (!queryString.isEmpty()) {
			final SolrQuery query = new SolrQuery();
			query.setQuery(queryString);
			if (sortField != null) {
				SortClause sortClause = new SortClause(sortField, order);
				query.setSort(sortClause);
			}
			query.setParam(CommonParams.START, String.valueOf(offset));
			query.setParam(CommonParams.ROWS, String.valueOf(limit));
			query.setParam(CommonParams.QT, this.queryHandlerMinimal);

			// send query to index
			try {
				response = this.server.query(this.coreName, query);
			} catch (final SolrServerException | IOException e) {
				throw new WebLabCheckedException("Cannot post search request for '" + queryString + "'.", e);
			}
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"The last query is empty dude ! What do you want me to do?");
		}

		return response;
	}

	/**
	 * Query the solr server to get the metadata associated to a list of document URIs
	 *
	 * @param uri : the list of URI
	 * @return a solr queryResponse that contain the relevant metadata associated to the documents
	 * @throws WebLabCheckedException is thrown if the query is not correctly handled
	 */
	public QueryResponse getMetaData(final String... uri) throws WebLabCheckedException {
		QueryResponse response = new QueryResponse();
		final StringBuilder buf = new StringBuilder();
		buf.append(SolrConfig.FIELD_ID + ":(");
		final String or = " OR ";
		for (int k = 0; k < uri.length; k++) {
			final String docURI = uri[k];
			buf.append('\"');
			buf.append(docURI);
			buf.append('\"');
			if (k != (uri.length - 1)) {
				buf.append(or);
			}
		}
		buf.append(")");

		final SolrQuery query = new SolrQuery();
		query.setQuery(buf.toString());

		query.setParam(CommonParams.START, "0");
		query.setParam(CommonParams.ROWS, String.valueOf(uri.length));
		query.setParam(CommonParams.QT, this.queryHandlerMeta);

		// send the query
		try {
			response = this.server.query(this.coreName, query);
		} catch (final SolrServerException | IOException e) {
			throw new WebLabCheckedException("Cannot post search request for " + buf.toString() + ".", e);
		}

		return response;
	}

	/**
	 * Highlight method that enrich search results with text content where the matching terms have been emphased.
	 *
	 * @param queryString is the full-text query in Lucene syntax
	 * @param offset is the rank of the first result to be returned
	 * @param limit is the size of the results list to be returned
	 * @param sortField The field used for sorting the results
	 * @param order Ascending or descending
	 * @return the Solr <code>QueryResponse</code> containing hits with highligted content
	 */
	public QueryResponse highlight(final String queryString, final int offset, final int limit, final String sortField, final ORDER order) {
		QueryResponse response = new QueryResponse();

		if (!queryString.isEmpty()) {
			final SolrQuery query = new SolrQuery();
			query.setQuery(queryString);

			if (sortField != null) {
				SortClause sortClause = new SortClause(sortField, order);
				query.setSort(sortClause);
			}

			query.setParam(CommonParams.START, String.valueOf(offset));
			query.setParam(CommonParams.ROWS, String.valueOf(limit));
			query.setParam(CommonParams.QT, this.queryHandlerHighLight);

			// send query to index
			try {
				response = this.server.query(this.coreName, query);
			} catch (final SolrServerException | IOException e) {
				this.logger.error("'<"+Constants.SERVICE_NAME+">'"+"Error processing document. "+"Cannot post highlight request", e);
			}
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"The input query is empty. Cannot highlight search results. What do you want me to do ?");
		}

		return response;
	}

	public QueryResponse spellSuggest(final String queryString) {
		QueryResponse response = new QueryResponse();
		if (!queryString.isEmpty()) {
			final SolrQuery query = new SolrQuery();
			query.setQuery(queryString);
			query.setParam(CommonParams.QT, this.queryHandlerSpell);

			// send query to index
			try {
				response = this.server.query(this.coreName, query);
			} catch (final SolrServerException | IOException e) {
				this.logger.error("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot post spell suggest request", e);
			}
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"The input query is empty. Cannot make spell suggestion.");
		}
		return response;
	}

	public QueryResponse facetSuggest(final String queryString, final int offset, final int limit, final String sortField, final ORDER order) {
		QueryResponse response = new QueryResponse();
		if (!queryString.isEmpty()) {
			final SolrQuery query = new SolrQuery();

			if (sortField != null) {
				SortClause sortClause = new SortClause(sortField, order);
				query.setSort(sortClause);
			}

			query.setQuery(queryString);
			query.setParam(CommonParams.QT, this.queryHandlerFacet);

			// send query to index
			try {
				response = this.server.query(this.coreName, query);
			} catch (final SolrServerException | IOException e) {
				this.logger.error("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot post spell suggest request", e);
			}
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"The input query is empty. Cannot make spell suggestion.");
		}
		return response;
	}

	/**
	 * MoreLikeThis is a specific Solr query used to retrieve similar documents from a document id or text. Source, title and text fields are used to find
	 * similar documents.
	 *
	 * @param queryString the reference text or document to find similar docs (id:"doc1" for example)
	 * @param offset The offset
	 * @param limit The limit (number of result to get)
	 * @return the Solr <code>QueryResponse</code> with document list
	 * @throws WebLabCheckedException If an error occurs
	 */
	public QueryResponse moreLikeThis(final String queryString, final int offset, final int limit) throws WebLabCheckedException {
		QueryResponse response = new QueryResponse();
		if (!queryString.isEmpty()) {
			final SolrQuery query = new SolrQuery();
			query.setParam(CommonParams.QT, this.queryHandlerMore);
			query.setParam(CommonParams.START, String.valueOf(offset));
			query.setParam(CommonParams.ROWS, String.valueOf(limit));

			// query.setQueryType(MoreLikeThisParams.MLT);
			query.set(MoreLikeThisParams.MLT);
			query.set(MoreLikeThisParams.MATCH_INCLUDE, false);
			query.set(MoreLikeThisParams.MIN_DOC_FREQ, 1);
			query.set(MoreLikeThisParams.MIN_TERM_FREQ, 1);
			query.set(MoreLikeThisParams.SIMILARITY_FIELDS, "source,title,text");
			query.setQuery(queryString);

			// Send query to index
			try {
				response = this.server.query(this.coreName, query);
			} catch (final SolrServerException | IOException e) {
				throw new WebLabCheckedException("Cannot post similar search request for '" + queryString + "'.", e);
			}
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+"Unable to properly process document. "+"The input query is empty. Cannot make a 'moreLikeThis' query.");
		}

		return response;
	}

	/**
	 * Ask for document deletion based on document URI
	 *
	 * @param docURI the URI of the document to delete.
	 * @return UpdateResponse : empty object just to check the deletion was done.
	 * @throws WebLabCheckedException in case of any problem.
	 */
	public UpdateResponse deleteDocbyURI(final String docURI) throws WebLabCheckedException {
		final StringBuffer query = new StringBuffer(SolrConfig.FIELD_ID);
		query.append(':');
		query.append('"');
		query.append(docURI);
		query.append('"');

		return this.deleteDocbyQuery(query.toString());
	}

	/**
	 * Ask for document deletion based on a query: all documents matching the query will be deleted.
	 *
	 * @param query a query to select documents
	 * @return UpdateResponse : empty object just to check the deletion was done.
	 * @throws WebLabCheckedException in case of any problem.
	 */
	public UpdateResponse deleteDocbyQuery(final String query) throws WebLabCheckedException {
		UpdateResponse response = new UpdateResponse();
		try {
			response = this.server.deleteByQuery(this.coreName, query.toString());
		} catch (final IOException | SolrServerException e) {
			throw new WebLabCheckedException("Request to delete failed for query [" + query + "].", e);
		}
		return response;
	}

	public String getCoreName() {
		return this.coreName;
	}

	public SolrConfig getConf() {
		return this.conf;
	}

	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}

}
