/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.solr.analyser;

import java.io.StringWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.services.solr.SolrComponent;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.SolrQueryParser;
import org.ow2.weblab.util.WebLabQueryParser;
import org.ow2.weblab.util.constants.Constants;
import org.ow2.weblab.util.index.Field;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class RDFFacetSuggestion implements Analyser {


	protected static final String WEBLAB_INDEXSEARCH_SOLR = "weblab://indexsearch.solr/";


	protected static final String FILTER_TAG = "/filter";


	public static final String BEAN_NAME = "rdfFacetSuggestionServiceBean";


	public static final String IDRES_FACET_PREFIX = "facets";


	protected static long suggestionCounter = 0;


	protected static long queryCounter = 0;


	protected Log logger;


	protected SolrConfig conf;


	private Set<Field> facetFields;


	private Set<Operator> appliedOperators;


	private final Map<String, String> fieldToPropertyMap = new HashMap<>();


	private WebLabQueryParser parser;


	@PostConstruct
	public void init() {
		this.logger = LogFactory.getLog(RDFFacetSuggestion.class);
		this.parser = new SolrQueryParser(this.conf);

		for (final Field f : this.facetFields) {
			if ((f.getProperties() != null) && (f.getProperties().size() > 0)) {
				// TODO find another way since it does not take into account the
				// mapping from field to property which is 1..*

				if ((f.getEntityTypes() != null) && (f.getEntityTypes().size() > 0)) {
					// use entity type filter as scope
					this.fieldToPropertyMap.put(f.getName(), f.getEntityTypes().get(0));
				} else {
					// use property filter as scope
					this.fieldToPropertyMap.put(f.getName(), f.getProperties().get(0));
				}
			}
		}
	}


	@PreDestroy
	public void destroy() {
		this.logger.info("Destroying SolR FacetSuggestion service.");
		// nothing to do I guess
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, UnexpectedException, InvalidParameterException,
			ContentNotAvailableException, InsufficientResourcesException, UnsupportedRequestException, ServiceNotConfiguredException {

		this.checkArgs(args);

		final ResultSet enrichSet = this.doFacetSuggest(args.getUsageContext(), (ResultSet) args.getResource());

		final ProcessReturn ret = new ProcessReturn();
		ret.setResource(enrichSet);
		return ret;
	}


	public ResultSet doFacetSuggest(final String usageContext, final ResultSet set) {

		try {
			if (!(set.getResource().get(0) instanceof Query)) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There is no query in the ResultSet.");
			}
			final Query query = (Query) set.getResource().get(0);

			final StringWriter writer = new StringWriter();
			new WebLabMarshaller().marshalResource(query, writer);
			this.logger.trace("faceting on : \n"+writer.toString());

			this.doFacetSuggest(usageContext, query, set);
		} catch (final WebLabCheckedException e) {
			this.logger.trace("Cannot make spell suggestion : " + e.getMessage());
		}
		return set;
	}


	public void doFacetSuggest(final String usageContext, final Query query, final ResultSet set) {
		try {
			SolrComponent instance;
			if (this.conf.isNoCore()) {
				instance = SolrComponent.getInstance(this.conf, null);
			} else {
				instance = SolrComponent.getInstance(this.conf, usageContext);
			}

			// check if the query has annotation to expect a specific behavior
			boolean ascOrder = false;
			String orderedByProperty = null;
			if (query.getAnnotation().size() > 0) {
				final ResourceHelper hlpr = new JenaResourceHelper(query);
				ascOrder = this.parser.getOrder(query, hlpr);
				orderedByProperty = this.parser.getOrderBy(query, hlpr);
			}
			final String request = this.parser.getRequest(query, new JenaResourceHelper(query));

			this.logger.trace("facet sugession on  request : "+request+" ordered by "+orderedByProperty);
			final QueryResponse response = instance.facetSuggest(request, 0, 1, orderedByProperty, ascOrder ? ORDER.asc : ORDER.desc);

			if ((response.getFacetFields() == null) || (response.getFacetFields().size() == 0)) {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"There are no facets in the ResultSet.");
			}

			WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(set.getUri()), set.getPok());
			for (final FacetField facet : response.getFacetFields()) {
				final List<Count> countList = facet.getValues();
				if (countList == null) {
					this.logger.trace("Facet " + facet.getName() + " is empty.");
				} else {
					for (final Count valuedFacet : countList) {
						final int nbRes = (int) valuedFacet.getCount();
						if (nbRes == 0) {
							this.logger.trace("Facet " + facet.getName() + ":" + valuedFacet.getName() + " is empty.");
						} else {
							try {
								final Query facetQuery = this.createFacetQuery(response.getResults().getNumFound(), query, 0, facet, nbRes,
										valuedFacet.getName(), wra);
								set.getResource().add(facetQuery);
							} catch (WebLabCheckedException e) {
								this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Ignoring a facet received: " + e.getMessage());
							}
						}
					}
				}
			}
			this.logger.trace("Facet suggestion done for [" + query.getUri() + "].");
		} catch (final WebLabCheckedException e) {
			this.logger.trace("Cannot make spell suggestion : " + e.getMessage(), e);
		}
	}


	/**
	 * Create facet query in accordance to the new usage of StringQuery => issue
	 * WEBLAB-316
	 *
	 * @param numFound
	 * 			   is the number of total documents found with the query
	 * @param query
	 *            is the original query that was sent to generate the facets
	 * @param offset
	 *            is the original offset of the query
	 * @param facet
	 *            is the facet as a FacetField
	 * @param nbRes
	 *            is the number of results associated to that facet if applied
	 *            as filter
	 * @param facetFilter
	 *            is the filter to apply as a String
	 * @return a StringQuery with all information related to scope and link in
	 *         an annotation.
	 * @throws WebLabCheckedException
	 * @deprecated Use the
	 */
	@Deprecated
	private Query createFacetQuery(final long numFound, final Query query, final int offset, final FacetField facet, final int nbRes, String facetFilter,
			WRetrievalAnnotator wra) throws WebLabCheckedException {
		final String filterIdRes = RDFFacetSuggestion.IDRES_FACET_PREFIX + SolrComponent.IDRES_QUERY_PREFIX + RDFFacetSuggestion.queryCounter++;

		final String facetSuggestionGenericUri = WEBLAB_INDEXSEARCH_SOLR + RDFFacetSuggestion.IDRES_FACET_PREFIX

		+ RDFFacetSuggestion.queryCounter;

		this.logger.trace("Creating facet "+facetFilter+" using "+facet+" with offset "+offset+" on "+numFound);

		// create facet filter
		final StringQuery facetQuery = WebLabResourceFactory
				.createResource(SolrComponent.IDREF, filterIdRes + RDFFacetSuggestion.FILTER_TAG, StringQuery.class);
		facetQuery.setWeight(Float.valueOf(1));
		// add scope on query to filter on specific field based on facet name
		// (trying to get proper property uri instead of field name)

		if (!this.fieldToPropertyMap.containsKey(facet.getName())) {
			throw new WebLabCheckedException("The facet [" + facet.getName() + "] is not known. Cannot process it.");
		}
		String scope = this.fieldToPropertyMap.get(facet.getName());
		this.logger.trace("Creating facet on [" + scope + "].");

		// WL Query + RDF query form
		final URI facetQueryUri = URI.create(facetQuery.getUri());
		final WRetrievalAnnotator hlpr = new WRetrievalAnnotator(facetQueryUri, WebLabResourceFactory.createAndLinkAnnotation(facetQuery));
		hlpr.writeScope(URI.create(scope));
		hlpr.writeLabel(facetFilter);

		wra.startInnerAnnotatorOn(facetQueryUri);
		wra.writeScope(URI.create(scope));
		wra.writeLabel(facetFilter);


		final String cleanFacet = facetFilter;

		// ensure expression are expressed with quotes
		if (!facetFilter.contains("\"")) {
			facetFilter = "\"" + facetFilter + "\"";
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"Facet filter proposed contain quotes [" + facetFilter + "]. This may lead to invalid query.");
		}
		// set filter
		facetQuery.setRequest(facetFilter);
		wra.applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.WRITE, "wl", URI.create(WebLabModel.HAS_REQUEST), String.class, facetQuery.getRequest());
		wra.writeType(URI.create(WebLabModel.STRING_QUERY));
		wra.endInnerAnnotator();

		URI queryUri = URI.create(query.getUri());
		Iterator<Operator> operatorIterator = this.appliedOperators.iterator();
		while (operatorIterator.hasNext()) {

			Operator nextOperator = operatorIterator.next();
			if (nextOperator.equals(Operator.NOT)) {
				// changed facet.getValueCount() with nbRes
				int negative = (int) (numFound - nbRes);
				populatePoKWithFacetSuggestion(wra, queryUri, facetSuggestionGenericUri, facetQueryUri, negative,
							cleanFacet, nextOperator);
			} else {

				populatePoKWithFacetSuggestion(wra, queryUri, facetSuggestionGenericUri, facetQueryUri, nbRes, cleanFacet, nextOperator);
			}

		}

		return facetQuery;
	}


	private void populatePoKWithFacetSuggestion(WRetrievalAnnotator wra, URI originalQueryUri, String facetSuggestionGenericUri, URI facetQueryUri, int valueCount,
			String facetFilter, Operator operator) {
		final URI facetSuggestionUri = URI.create(facetSuggestionGenericUri + operator);
		wra.startInnerAnnotatorOn(originalQueryUri);
		wra.writeFacetSuggestion(facetSuggestionUri);
		wra.endInnerAnnotator();
		wra.startInnerAnnotatorOn(facetSuggestionUri);
		wra.writeLinkedTo(facetQueryUri);
		wra.writeType(URI.create(WebLabRetrieval.FACET_SUGGESTION));
		wra.writeLabel(facetFilter);
		wra.applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.WRITE, "wl", URI.create(WebLabModel.HAS_OPERATOR), String.class, operator.toString());
		wra.writeNumberOfResults(Integer.valueOf(valueCount));
		wra.endInnerAnnotator();
	}


	private void checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "Input args for [" + this.getClass().getSimpleName() + "] cannot be null.";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+message);
		}
		// TODO finish it...
	}


	public SolrConfig getConf() {
		return this.conf;
	}


	public void setConf(final SolrConfig conf) {
		this.conf = conf;
	}


	public Set<Field> getFacetFields() {
		return this.facetFields;
	}


	public void setFacetFields(final Set<Field> facetFields) {
		this.facetFields = facetFields;
	}


	public Set<Operator> getAppliedOperators() {
		return this.appliedOperators;
	}


	public void setAppliedOperators(Set<Operator> appliedOperators) {
		this.appliedOperators = appliedOperators;
	}

}
