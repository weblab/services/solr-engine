/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.util;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Query;

public abstract class WebLabQueryParser {


	/**
	 * Build a string request based on the content of a Query.
	 *
	 * @param q
	 *            a Query
	 * @param hlpr
	 *            a JenaResourceHelper initialized on the Query
	 * @return the request as a string
	 * @throws WebLabCheckedException If an error occurs
	 */
	public abstract String getRequest(Query q, ResourceHelper hlpr) throws WebLabCheckedException;


	/**
	 * Build a string query based on the content of a ComposedQuery.
	 *
	 * This method is for now limited to simple composition (ie one level
	 * ComposedQuery) and only accepting StringQuery as sub- queries.
	 *
	 * @param q
	 *            the ComposedQuery
	 * @param hlpr
	 *            a JenaResourceHelper initialized on the ComposedQuery
	 * @return the composed request as a string
	 * @throws WebLabCheckedException If an error occurs
	 */
	public abstract String getComposedRequest(ComposedQuery q, ResourceHelper hlpr) throws WebLabCheckedException;


	/**
	 * Extract the request from the StringQuery possibly using the property
	 * HAS_SCOPE to restrict request scope and adapt syntax.
	 *
	 * @param q
	 *            a StrinQuery
	 * @param hlpr the resource helper
	 * @return the request as a String
	 * @throws WebLabCheckedException If an error occurs
	 */
	public abstract String getRequestWithScope(Query q, ResourceHelper hlpr) throws WebLabCheckedException;


	/**
	 * Test if the property TO_BE_ORDERED_BY exists on the query and get its
	 * value if applicable.
	 *
	 * @param q
	 *            a StrinQuery
	 * @param hlpr The resource helper
	 * @return The URI of the property to be used for ordering or null if empty.
	 * @throws WebLabCheckedException If an error occurs
	 */
	public abstract String getOrderBy(Query q, ResourceHelper hlpr) throws WebLabCheckedException;


	/**
	 * Test if the property ASCENDENT_ORDERING_MODE_EXPECTED is set to true.
	 *
	 * @param q
	 *            a StrinQuery
	 * @param hlpr the resource helper
	 * @return true of false, the value of the property (default is false)
	 * @throws WebLabCheckedException If an error occurs
	 */
	public abstract boolean getOrder(Query q, ResourceHelper hlpr) throws WebLabCheckedException;

}
