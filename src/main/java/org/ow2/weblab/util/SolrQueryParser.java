/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.util;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.SimilarityQuery;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.services.solr.SolrConfig;
import org.ow2.weblab.util.constants.Constants;
import org.ow2.weblab.util.index.Field;

public class SolrQueryParser extends WebLabQueryParser {


	protected final Log logger;


	private final SolrConfig config;


	public SolrQueryParser(final SolrConfig config) {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.config = config;
	}


	@Override
	public String getRequest(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		if (q instanceof StringQuery) {
			// add scope support on string query
			return  this.getRequestWithScope(q, hlpr);
		} else if (q instanceof ComposedQuery) {
			return this.getComposedRequest((ComposedQuery) q, hlpr);
		} else if (q instanceof SimilarityQuery) {
			final SimilarityQuery query = (SimilarityQuery) q;
			final StringBuffer queryString = new StringBuffer();
			final List<Resource> resources = query.getResource();
			queryString.append('(');
			if (resources.size() == 1) {
				// single sample => can use more like this function
				queryString.append(SolrConfig.FIELD_ID);
				queryString.append(':');
				queryString.append('"');
				queryString.append(resources.get(0).getUri());
				queryString.append('"');
			} else {
				// multiple samples, thus try simple ID based query
				// TODO check this is the expected behavior
				for (int i = 0; i < resources.size(); i++) {
					final Resource r = resources.get(i);
					if (i > 0) {
						queryString.append(" OR ");
					}
					queryString.append(SolrConfig.FIELD_ID);
					queryString.append(':');
					queryString.append('"');
					queryString.append(r.getUri());
					queryString.append('"');
				}
			}
			queryString.append(')');
			return queryString.toString();
		}
		throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Cannot parse query type : " + q.getClass() + ".");
	}


	@Override
	public String getComposedRequest(final ComposedQuery q, final ResourceHelper hlpr) throws WebLabCheckedException {
		// Watch out : this supposes that WebLab Operator are following Lucene
		// operator syntax...
 		final String operator = q.getOperator().value();

		final StringBuffer queryString = new StringBuffer();
		queryString.append('(');
		int index = 0;
		for (final Query subQ : q.getQuery()) {
			// correct to match composed query specification
			if (Operator.NOT.equals(q.getOperator()) && (index > 0 || (index == 0 && q.getQuery().size()==1))) {
				queryString.append("  " + operator + " ");
			} else if  (queryString.length() > 1) {
				queryString.append(' ');
				queryString.append(operator);
				queryString.append(' ');
			}

			if (subQ instanceof StringQuery) {
				queryString.append(this.getRequestWithScope(subQ, hlpr));
			} else if (subQ instanceof ComposedQuery) {
				queryString.append(this.getComposedRequest((ComposedQuery) subQ, hlpr));
			} else {
				throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Solr engine service can only process " + ComposedQuery.class.getSimpleName() + " or "
						+ StringQuery.class.getSimpleName() + ".");
			}
			index++;
		}
		queryString.append(')');
		return queryString.toString();
	}


	@Override
	public String getRequestWithScope(final Query query, final ResourceHelper hlpr) throws WebLabCheckedException {
		final List<String> values = hlpr.getRessOnPredSubj(query.getUri(), WebLabRetrieval.HAS_SCOPE);
		if (values.size() == 0) {
			this.logger.trace("Query has no scope.");
		} else if ((values.size() > 1) && (new HashSet<>(values).size() > 1)) {
			this.logger.trace("Query is holding multiple values for [" + WebLabRetrieval.HAS_SCOPE + "]. That's not cool, so we ignore all of them.");
		} else if (!this.config.getPropertyToFieldMap().containsKey(values.get(0))) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process document. "+"The value for [" + WebLabRetrieval.HAS_SCOPE + "] is [" + values.get(0)
					+ "] which is unknown from SolrIndexerConfig. That's not cool, so we ignore it.");
		} else {
			final Field field = this.config.getPropertyToFieldMap().get(values.get(0));
			if(query instanceof StringQuery){
				return field.getName() + SolrConfig.FIELD_SEPARATOR + "(" + ((StringQuery)query).getRequest() + ")";
			}
			return field.getName() + SolrConfig.FIELD_SEPARATOR + "(" + this.getRequest(query, hlpr) + ")";
		}
		if(query instanceof StringQuery){
			return "(" + ((StringQuery)query).getRequest() + ")";
		}
		return this.getRequest(query, hlpr);
	}


	@Override
	public String getOrderBy(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		final List<String> values = hlpr.getRessOnPredSubj(q.getUri(), WebLabRetrieval.TO_BE_RANKED_BY);
		if (values.size() == 0) {
			this.logger.trace("Query has no 'order by' part.");
		} else if ((values.size() > 1) && (new HashSet<>(values).size() > 1)) {
			this.logger.trace("Query is holding multiple values for [" + WebLabRetrieval.TO_BE_RANKED_BY + "]. That's not funky, so we ignore all of them.");
		} else {
			if (this.config != null) {
				return this.config.getPropertyToFieldMap().get(values.get(0)).getName();
			}
		}
		return null;
	}


	@Override
	public boolean getOrder(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		final List<String> values = hlpr.getLitsOnPredSubj(q.getUri(), WebLabRetrieval.TO_BE_RANKED_ASCENDING);
		if (values.size() == 0) {
			this.logger.trace("Query has no ordering specified.");
		} else if ((values.size() > 1) && (new HashSet<>(values).size() > 1)) {
			this.logger.trace("Query is holding multiple values for [" + WebLabRetrieval.TO_BE_RANKED_ASCENDING
					+ "]. That's not well perceived, so we ignore all of them.");
		} else {
			return Boolean.parseBoolean(values.get(0));
		}
		return false;
	}

}
