/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.util.index;

import java.util.LinkedList;
import java.util.List;

public final class Field {


	/**
	 * Name of the field in the index
	 */
	private String name;


	/**
	 * Field type : should be text, number, date...
	 */
	private FieldType type = FieldType.TEXT;


	/**
	 * Field indexing boost used in final scoring of documents, default being 1.0 (i.e. no boost)
	 */
	private float boost = 1.0f;


	/**
	 * Indicate if the field contain MediaUnit text parts
	 */
	private boolean indexTextContent = false;


	/**
	 * Indicate if the field must index the uri of the semantic entity in the case of this is a recognized one (not a candidate)
	 */
	private boolean indexNonCandidateInstanceAsUri = false;


	/**
	 * Indicate if the field must index
	 */
	private boolean indexTypeOnly = false;


	/**
	 * List of properties indexed in this field
	 */
	private List<String> properties;

	/**
	 * Restrict the RDF properties that have as subject the current weblab document
	 */
	private boolean restrictSubjectToDocument = false;

	/**
	 * List of valid entity types that could be subject of the properties indexed
	 */
	private List<String> entityTypes;


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name.trim();
	}


	/**
	 * @return the type
	 */
	public FieldType getType() {
		return this.type;
	}


	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(final FieldType type) {
		this.type = type;
	}


	public boolean isIndexTextContent() {
		return this.indexTextContent;
	}


	public void setIndexTextContent(final boolean indexTextContent) {
		this.indexTextContent = indexTextContent;
	}


	public boolean isIndexNonCandidateInstanceAsUri() {
		return this.indexNonCandidateInstanceAsUri;
	}


	public void setIndexNonCandidateInstanceAsUri(final boolean indexNonCandidateInstanceAsUri) {
		this.indexNonCandidateInstanceAsUri = indexNonCandidateInstanceAsUri;
	}


	public boolean isIndexTypeOnly() {
		return this.indexTypeOnly;
	}


	public void setIndexTypeOnly(final boolean indexTypeOnly) {
		this.indexTypeOnly = indexTypeOnly;
	}


	/**
	 * @return the entityTypes
	 */
	public List<String> getProperties() {
		return this.properties;
	}


	/**
	 * @param properties
	 *            The properties
	 */
	public void setProperties(final List<String> properties) {
		this.properties = new LinkedList<>();
		for (final String property : properties) {
			this.properties.add(property.trim());
		}
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + (this.indexTextContent ? 1231 : 1237);
		result = (prime * result) + ((this.name == null) ? 0 : this.name.hashCode());
		result = (prime * result) + ((this.properties == null) ? 0 : this.properties.hashCode());
		result = (prime * result) + ((this.type == null) ? 0 : this.type.hashCode());
		return result;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Field other = (Field) obj;
		if (this.indexTextContent != other.indexTextContent) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.properties == null) {
			if (other.properties != null) {
				return false;
			}
		} else if (!this.properties.equals(other.properties)) {
			return false;
		}
		if (this.type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!this.type.equals(other.type)) {
			return false;
		}
		return true;
	}


	@Override
	public String toString() {
		return "Field [indexTextMediaUnit=" + this.indexTextContent + ", name=" + this.name + ", properties=" + this.properties + ", type=" + this.type + "]";
	}


	public boolean isRestrictSubjectToDocument() {
		return this.restrictSubjectToDocument;
	}


	public void setRestrictSubjectToDocument(boolean restrictSubjectToDocument) {
		this.restrictSubjectToDocument = restrictSubjectToDocument;
	}

	public List<String> getEntityTypes() {
		return this.entityTypes;
	}


	public void setEntityTypes(final List<String> entityTypes) {
		this.entityTypes = entityTypes;
	}


	public float getBoost() {
		return this.boost;
	}


	public void setBoost(final float boost) {
		this.boost = boost;
	}


	/**
	 * Field types defined in the default SolR schema for WebLab.
	 *
	 * No ! We do not need more than that ;-)
	 *
	 * @author gdupont - WebLab team Airbus Defence and Space
	 */
	public enum FieldType {
		TEXT("text"), DATE("date"), INTEGER("sint"), LONG("slong"), RES_URI("uri"), URI("uri"), DOUBLE("sdouble"), BOOLEAN("boolean");


		private final String type;


		private FieldType(final String type) {
			this.type = type;
		}


		@Override
		public String toString() {
			return this.type;
		}
	}
}
