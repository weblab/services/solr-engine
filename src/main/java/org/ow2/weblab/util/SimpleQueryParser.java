/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.util;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.util.constants.Constants;

/**
 * Only tackle StringQuery and ComposedQuery. Does not take any annotation into account
 *
 * @author gdupont - WebLab team ; CASSIDIAN, an EADS company
 */
public class SimpleQueryParser extends WebLabQueryParser {


	@Override
	public String getRequest(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		if (q instanceof StringQuery) {
			final StringQuery squery = (StringQuery) q;
			return squery.getRequest();
		} else if (q instanceof ComposedQuery) {
			final ComposedQuery cquery = (ComposedQuery) q;
			return this.getComposedRequest(cquery, hlpr);
		}
		return null;
	}


	@Override
	public String getComposedRequest(final ComposedQuery q, final ResourceHelper hlpr) throws WebLabCheckedException {
		final StringBuffer queryString = new StringBuffer();
		queryString.append('(');
		for (final Query subQ : q.getQuery()) {
			if (queryString.length() > 1) {
				queryString.append(' ');
			}

			final String rq = this.getRequest(subQ, hlpr);

			if (rq != null) {
				queryString.append(rq);
			}

		}
		queryString.append(')');
		return queryString.toString();
	}


	@Override
	public String getRequestWithScope(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Not implemented.");
	}


	@Override
	public String getOrderBy(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Not implemented.");
	}


	@Override
	public boolean getOrder(final Query q, final ResourceHelper hlpr) throws WebLabCheckedException {
		throw new WebLabCheckedException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+"Not implemented.");
	}

}
